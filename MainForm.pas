unit MainForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.ComCtrls
  , Vcl.Menus
  , Models
  ;

type
  TMain = class(TForm)
    sbStatus: TStatusBar;
    mmMain: TMainMenu;
    miUser: TMenuItem;
    miMaintenance: TMenuItem;
    miUserLogin: TMenuItem;
    miUserClose: TMenuItem;
    miMaintenanceUser: TMenuItem;
    miMaintenanceRoles: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miUserLoginClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miMaintenanceUserClick(Sender: TObject);
    procedure miMaintenanceRolesClick(Sender: TObject);
  private
    FModels: TModels;
    procedure UpdateForm;
  public
  end;

var
  Main: TMain;

implementation

uses
    Constants
  , LoginForm
  , UsersViewForm
  , RolesViewForm
  ;

{$R *.dfm}

procedure TMain.FormCreate(Sender: TObject);
begin
  Main.Caption := PROJECT_NAME;
  FModels := TModels.Create;
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  if Assigned(FModels) then FreeAndNil(FModels);
end;

procedure TMain.FormShow(Sender: TObject);
begin
  FModels.Global.Status.UserID := -1;
  if TLogin.Execute(FModels) then
    UpdateForm
  else
    Application.Terminate;
end;

procedure TMain.miMaintenanceRolesClick(Sender: TObject);
begin
  TRolesView.Execute(FModels);
end;

procedure TMain.miMaintenanceUserClick(Sender: TObject);
begin
  TUsersView.Execute(FModels);
end;

procedure TMain.miUserLoginClick(Sender: TObject);
begin
  TLogin.Execute(FModels);
end;

procedure TMain.UpdateForm;
begin
  FModels.UsersPrivileges.SetUser(FModels.Global.Status.UserID);

  sbStatus.Panels[0].Text := FModels.Global.Status.Username;
  sbStatus.Panels[1].Text := FModels.Global.Status.Database;
  sbStatus.Panels[2].Text := FModels.Global.Version.DBVersion + ' - ' +
    FModels.Global.Version.AppVersion;

  miMaintenanceUser.Visible := FModels.Global.UserPrivileges.HasUsersView;
  miMaintenanceRoles.Visible := FModels.Global.UserPrivileges.HasRolesView;
  miMaintenance.Visible :=
    miMaintenanceUser.Visible or
    miMaintenanceRoles.Visible;
end;

end.
