unit UsersPrivilegesModel;

interface

uses
    BaseModel
  , Database
  , Global
  , ZDataset
  ;

{$M+}

type
  TUsersPrivilegesModel = class(TBaseModel)
    procedure DefaultSettings; override;
    procedure SelfTest; override;
  private
  public
    function SetUser(const AUserID: Integer): Boolean;
  published
  end;

implementation

uses
    Constants
  ;

{ TUsersPrivilegesModel }

procedure TUsersPrivilegesModel.DefaultSettings;
begin
  Global.UserPrivileges.HasSettingsView := False;
  Global.UserPrivileges.HasSettingsUpdate := False;
  Global.UserPrivileges.HasUsersView := False;
  Global.UserPrivileges.HasUsersInsert := False;
  Global.UserPrivileges.HasUsersUpdate := False;
  Global.UserPrivileges.HasUsersDelete := False;
  Global.UserPrivileges.HasRolesView := False;
  Global.UserPrivileges.HasRolesInsert := False;
  Global.UserPrivileges.HasRolesUpdate := False;
  Global.UserPrivileges.HasRolesDelete := False;
end;

procedure TUsersPrivilegesModel.SelfTest;
var
  Privileges: Array of String;
  i, ID: Integer;
begin
  Privileges :=
    [PRIVILEGE_SETTINGS_VIEW] +
    [PRIVILEGE_SETTINGS_UPDATE] +
    [PRIVILEGE_USERS_VIEW] +
    [PRIVILEGE_USERS_INSERT] +
    [PRIVILEGE_USERS_UPDATE] +
    [PRIVILEGE_USERS_DELETE] +
    [PRIVILEGE_ROLES_VIEW] +
    [PRIVILEGE_ROLES_INSERT] +
    [PRIVILEGE_ROLES_UPDATE] +
    [PRIVILEGE_ROLES_DELETE];

  Assert(DB.Parties.Insert > 0, 'insert new party');
  Assert(DB.Users.Insert(DB.Parties.LastID,'username','test') > 0, 'insert new user');
  Assert(DB.Roles.Insert('TestRole') > 0, 'insert new role');
  Assert(DB.UserRoles.Insert(DB.Parties.LastID, DB.Roles.LastID), 'assign new role to new user');

  //add one privilege at a time and test
  for i := 0 to Length(Privileges) - 1 do
  begin
    Assert(not Global.UserPrivileges.HasSettingsView, 'test default value');
    Assert(not Global.UserPrivileges.HasSettingsUpdate, 'test default value');
    Assert(not Global.UserPrivileges.HasUsersView, 'test default value');
    Assert(not Global.UserPrivileges.HasUsersInsert, 'test default value');
    Assert(not Global.UserPrivileges.HasUsersUpdate, 'test default value');
    Assert(not Global.UserPrivileges.HasUsersDelete, 'test default value');
    Assert(not Global.UserPrivileges.HasRolesView, 'test default value');
    Assert(not Global.UserPrivileges.HasRolesInsert, 'test default value');
    Assert(not Global.UserPrivileges.HasRolesUpdate, 'test default value');
    Assert(not Global.UserPrivileges.HasRolesDelete, 'test default value');

    ID := DB.Privileges.GetID(Privileges[i]);
    Assert(ID > 0, 'check if a valid ID was retreived');
    Assert(DB.RolePrivileges.Insert(DB.Roles.LastID, ID), 'add ' + Privileges[i] + ' to the new role');
    Assert(SetUser(DB.Parties.LastID), 'load users privileges');

    //this looks awful, but i cant think of any other way
    if i = 0 then Assert(Global.UserPrivileges.HasSettingsView, 'test if privilege added')
    else if i = 1 then Assert(Global.UserPrivileges.HasSettingsUpdate, 'test if privilege added')
    else if i = 2 then Assert(Global.UserPrivileges.HasUsersView, 'test if privilege added')
    else if i = 3 then Assert(Global.UserPrivileges.HasUsersInsert, 'test if privilege added')
    else if i = 4 then Assert(Global.UserPrivileges.HasUsersUpdate, 'test if privilege added')
    else if i = 5 then Assert(Global.UserPrivileges.HasUsersDelete, 'test if privilege added')
    else if i = 6 then Assert(Global.UserPrivileges.HasRolesView, 'test if privilege added')
    else if i = 7 then Assert(Global.UserPrivileges.HasRolesInsert, 'test if privilege added')
    else if i = 8 then Assert(Global.UserPrivileges.HasRolesUpdate, 'test if privilege added')
    else if i = 9 then Assert(Global.UserPrivileges.HasRolesDelete, 'test if privilege added');

    if not i = 0 then Assert(not Global.UserPrivileges.HasSettingsView, 'test default value');
    if not i = 1 then Assert(not Global.UserPrivileges.HasSettingsUpdate, 'test default value');
    if not i = 2 then Assert(not Global.UserPrivileges.HasUsersView, 'test default value');
    if not i = 3 then Assert(not Global.UserPrivileges.HasUsersInsert, 'test default value');
    if not i = 4 then Assert(not Global.UserPrivileges.HasUsersUpdate, 'test default value');
    if not i = 5 then Assert(not Global.UserPrivileges.HasUsersDelete, 'test default value');
    if not i = 6 then Assert(not Global.UserPrivileges.HasRolesView, 'test default value');
    if not i = 7 then Assert(not Global.UserPrivileges.HasRolesInsert, 'test default value');
    if not i = 8 then Assert(not Global.UserPrivileges.HasRolesUpdate, 'test default value');
    if not i = 9 then Assert(not Global.UserPrivileges.HasRolesDelete, 'test default value');

    Assert(DB.RolePrivileges.Delete(DB.Roles.LastID, ID), 'delete ' + Privileges[i] + ' from the new role');

    DefaultSettings;
  end;

  Assert(DB.UserRoles.Delete(DB.Parties.LastID, DB.Roles.LastID), 'delete new role from new user');
  Assert(DB.Roles.Delete(DB.Roles.LastID), 'delete new role');
  Assert(DB.Users.Delete(DB.Parties.LastID), 'delete new user');
  Assert(DB.Parties.Delete(DB.Parties.LastID), 'delete new party');

  DefaultSettings; //return to default settings
end;

function TUsersPrivilegesModel.SetUser(const AUserID: Integer): Boolean;
var
  SQL: TZQuery;
begin
  Result := True;
  SQL := DB.UsersUserRolesRolesRolePrivilegesPrivileges.GetUsersPrivileges(AUserID);
  if SQL = nil then
    Exit;

  DefaultSettings;

  while not SQL.Eof do begin
    if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_SETTINGS_VIEW then
      Global.UserPrivileges.HasSettingsView := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_SETTINGS_UPDATE then
      Global.UserPrivileges.HasSettingsUpdate := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_USERS_VIEW then
      Global.UserPrivileges.HasUsersView := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_USERS_INSERT then
      Global.UserPrivileges.HasUsersInsert := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_USERS_UPDATE then
      Global.UserPrivileges.HasUsersUpdate := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_USERS_DELETE then
      Global.UserPrivileges.HasUsersDelete := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_ROLES_VIEW then
      Global.UserPrivileges.HasRolesView := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_ROLES_INSERT then
      Global.UserPrivileges.HasRolesInsert := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_ROLES_UPDATE then
      Global.UserPrivileges.HasRolesUpdate := True
    else if SQL.FieldByName(PRIVILEGES_DB_NAME).AsString = PRIVILEGE_ROLES_DELETE then
      Global.UserPrivileges.HasRolesDelete := True;
    SQL.Next;
  end;
  SQL.Free;
end;

end.
