unit UsersInsertUpdateModel;

interface

uses
    BaseModel
  , ProjectTypes
  , Global
  ;

{$M+}

type
  TUsersInsertUpdateModel = class(TBaseModel)
    procedure DefaultSettings; override;
    procedure SelfTest; override;
    procedure SetPrivileges(const AUsersPrivileges: TUsersPrivileges);
    function SelectAllUsersAvailableRolesCreateArray(
      const AUserID: Integer): TRoles;
    function SelectAllUsersRolesCreateArray(const AUserID: Integer): TRoles;
  private
    FMode: TUsersInsertUpdateModes;
    FOriginalUserDetails: TUserDetails;
    FUserDetails: TUserDetails;
    FNewPassword: String;
    FConfirmNewPassword: String;
    FAvailableRoles: TRoles;
    procedure FreeAssignedRoles;
    procedure FreeAvailableRoles;
    procedure SetGivenName(const Value: String);
    procedure SetIsActive(const Value: Boolean);
    procedure SetSurname(const Value: String);
    procedure SetUsername(const Value: String);
  public
    destructor Destroy; override;
    procedure Initialize(const AMode: TUsersInsertUpdateModes;
      const AUserDetails: TUserDetails);
    procedure RefreshLists;
    function Save: Boolean;
    procedure AssignAllRoles;
    procedure AssignRole(const AIndex: Integer);
    procedure RemoveAllRoles;
    procedure RemoveRole(const AIndex: Integer);
    function IsBtnAssignAllEnabled: Boolean;
    function IsBtnAssignEnabled(const AIndex: Integer): Boolean;
    function IsBtnRemoveEnabled(const AIndex: Integer): Boolean;
    function IsBtnRemoveAllEnabled: Boolean;
  published
    property UserDetails: TUserDetails read FUserDetails;
    property AvailableRoles: TRoles read FAvailableRoles;
    property NewPassword: String write FNewPassword;
    property ConfirmNewPassword: String write FConfirmNewPassword;
    property GivenName: String write SetGivenName;
    property Surname: String write SetSurname;
    property Username: String write SetUsername;
    property IsActive: Boolean write SetIsActive;
  end;

implementation

uses
    System.SysUtils
  , Utils
  ;

{ TUsersInsertUpdateModel }

procedure TUsersInsertUpdateModel.AssignAllRoles;
begin
  FUserDetails.Roles := FUserDetails.Roles + FAvailableRoles;
  FAvailableRoles := nil;
end;

procedure TUsersInsertUpdateModel.AssignRole(const AIndex: Integer);
var
  i: Integer;
  newList: TRoles;
begin
  if AIndex > Length(FAvailableRoles) - 1 then Exit;

  for i := 0 to Length(FAvailableRoles) - 1 do
  begin
    if i = AIndex then
      FUserDetails.Roles := FUserDetails.Roles + [FAvailableRoles[i]]
    else
      newList := newList + [FAvailableRoles[i]];
  end;
  FAvailableRoles := nil;
  FAvailableRoles := newList;
end;

procedure TUsersInsertUpdateModel.DefaultSettings;
begin
  inherited;

end;

destructor TUsersInsertUpdateModel.Destroy;
begin
  FreeAssignedRoles;
  FreeAvailableRoles;
  inherited;
end;

procedure TUsersInsertUpdateModel.FreeAssignedRoles;
var
  i: Integer;
begin
  for i := Length(FUserDetails.Roles) - 1 downto 0 do
    if Assigned(FUserDetails.Roles[i]) then FreeAndNil(FUserDetails.Roles[i]);
  FUserDetails.Roles := nil;
end;

procedure TUsersInsertUpdateModel.FreeAvailableRoles;
var
  i: Integer;
begin
  for i := Length(FAvailableRoles) - 1 downto 0 do
    if Assigned(FAvailableRoles[i]) then FreeAndNil(FAvailableRoles[i]);
  FAvailableRoles := nil;
end;

procedure TUsersInsertUpdateModel.Initialize(
  const AMode: TUsersInsertUpdateModes; const AUserDetails: TUserDetails);
var
  i: Integer;
  Roles: TRoles;
begin
  FreeAssignedRoles;
  FreeAvailableRoles;
  SetPrivileges(FGlobal.UserPrivileges);
  FMode := AMode;
  FOriginalUserDetails := AUserDetails;
  //default values
  FUserDetails.PartyID := -1;
  FUserDetails.GivenName := '';
  FUserDetails.Surname := '';
  FUserDetails.Username := '';
  FUserDetails.IsActive := True;
  case FMode of
    umNew:
      begin
        FAvailableRoles := SelectAllUsersAvailableRolesCreateArray(-1);
      end;
    umDuplicate:
      begin
        Roles := SelectAllUsersAvailableRolesCreateArray(-1);
        for i := 0 to Length(Roles) - 1 do
        begin
          if Roles[i].Find(AUserDetails.Roles) then
            FUserDetails.Roles := FUserDetails.Roles + [Roles[i]]
          else
            FAvailableRoles := FAvailableRoles + [Roles[i]];
        end;
      end;
    umEdit:
      begin
        FUserDetails.PartyID := AUserDetails.PartyID;
        FUserDetails.GivenName := AUserDetails.GivenName;
        FUserDetails.Surname := AUserDetails.Surname;
        FUserDetails.Username := AUserDetails.Username;
        FUserDetails.IsActive := AUserDetails.IsActive;
        Roles := SelectAllUsersAvailableRolesCreateArray(-1);
        for i := 0 to Length(Roles) - 1 do
        begin
          if Roles[i].Find(AUserDetails.Roles) then
            FUserDetails.Roles := FUserDetails.Roles + [Roles[i]]
          else
            FAvailableRoles := FAvailableRoles + [Roles[i]];
        end;
      end;
  end;
end;

function TUsersInsertUpdateModel.IsBtnAssignAllEnabled: Boolean;
begin
  Result := Length(FAvailableRoles) > 0;
end;

function TUsersInsertUpdateModel.IsBtnAssignEnabled(
  const AIndex: Integer): Boolean;
begin
  Result := IsBtnAssignAllEnabled and (AIndex >= 0);
end;

function TUsersInsertUpdateModel.IsBtnRemoveAllEnabled: Boolean;
begin
  Result := Length(FUserDetails.Roles) > 0;
end;

function TUsersInsertUpdateModel.IsBtnRemoveEnabled(
  const AIndex: Integer): Boolean;
begin
  Result := IsBtnRemoveAllEnabled and (AIndex >= 0);
end;

//this should be able to handle sudden additional rows to roles table
procedure TUsersInsertUpdateModel.RefreshLists;
var
  i: Integer;
  Roles: TRoles;
  NewAssignedRoles: TRoles;
begin
  FreeAvailableRoles;
  Roles := SelectAllUsersAvailableRolesCreateArray(-1); //get all roles
  for i := 0 to Length(Roles) - 1 do
  begin
    if Roles[i].Find(FUserDetails.Roles) then
      NewAssignedRoles := NewAssignedRoles + [Roles[i]]
    else
      FAvailableRoles := FAvailableRoles + [Roles[i]];
  end;
  FreeAssignedRoles;
  FUserDetails.Roles := NewAssignedRoles;
end;

procedure TUsersInsertUpdateModel.RemoveAllRoles;
begin
  FAvailableRoles := FAvailableRoles + FUserDetails.Roles;
  FUserDetails.Roles := nil;
end;

procedure TUsersInsertUpdateModel.RemoveRole(const AIndex: Integer);
var
  i: Integer;
  newList: TRoles;
begin
  if AIndex > Length(FUserDetails.Roles) - 1 then Exit;

  for i := 0 to Length(FUserDetails.Roles) - 1 do
  begin
    if i = AIndex then
      FAvailableRoles := FAvailableRoles + [FUserDetails.Roles[i]]
    else
      newList := newList + [FUserDetails.Roles[i]];
  end;
  FUserDetails.Roles := nil;
  FUserDetails.Roles := newList;
end;

function TUsersInsertUpdateModel.Save: Boolean;
  function IsUsernameUnique(const ASearch: String): Boolean;
  begin
    Result := False;
    if not HasViewPrivileges then exit;

    Result := DB.Users.IsUsernameUnique(ASearch);
  end;

  function IsRequiredFieldsValid: Boolean;
  var
    Msg: String;
  begin
    Result := True;
    if Length(FUserDetails.GivenName) = 0 then
    begin
      Result := False;
      Msg := Msg + 'Given name can''t be blank.' + sLineBreak;
    end;
    if Length(FUserDetails.Surname) = 0 then
    begin
      Result := False;
      Msg := Msg + 'Surname can''t be blank.' + sLineBreak;
    end;
    if Length(FUserDetails.Username) = 0 then
    begin
      Result := False;
      Msg := Msg + 'Username can''t be blank.' + sLineBreak;
    end;
    if (not IsUsernameUnique(FUserDetails.Username) and
      (FUserDetails.Username <> FOriginalUserDetails.Username)) then
    begin
      Result := False;
      Msg := Msg + 'Username must be unique.' + sLineBreak;
    end;
    if (FMode <> umEdit) and (Length(FNewPassword) = 0) then
    begin
      Result := False;
      Msg := Msg + 'Password can''t be blank.' + sLineBreak;
    end;
    if FNewPassword <> FConfirmNewPassword then
    begin
      Result := False;
      Msg := Msg + 'Passwords must match' + sLineBreak;
    end;

    if not Result then
      Utils.MsgWarning(Msg);
  end;

  function Insert: Boolean;
  var
    i, NewPartyID: Integer;
  begin
    Result := False;
    if not IsRequiredFieldsValid then Exit;

    NewPartyID := DB.Parties.Insert;
    if NewPartyID > 0 then
    begin
      if DB.Persons.Insert(
        NewPartyID,
        FUserDetails.GivenName,
        FUserDetails.Surname) <= 0 then Exit
    end
    else
      Exit;

    if DB.Users.Insert(NewPartyID, FUserDetails.Username, FNewPassword) < 0 then
      Exit;

    if not DB.Users.SetIsActive(NewPartyID, FUserDetails.IsActive) then
      Exit;

    for i := 0 to Length(FUserDetails.Roles) - 1 do
    begin
      if not DB.UserRoles.Insert(NewPartyID,FUserDetails.Roles[i].ID) then Exit;
    end;

    Result := True;
  end;

  function Update: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if not IsRequiredFieldsValid then Exit;
    if FUserDetails.PartyID <= 0 then Exit;

    if (FOriginalUserDetails.GivenName <> FUserDetails.GivenName) or
      (FOriginalUserDetails.Surname <> FUserDetails.Surname) then
      if not DB.Persons.Edit(FUserDetails.PartyID,FUserDetails.GivenName,
        FUserDetails.Surname) then Exit;

    if (FOriginalUserDetails.Username <> FUserDetails.Username) then
      if not DB.Users.EditUsername(FUserDetails.PartyID,FUserDetails.Username) then
        Exit;
    if Length(FNewPassword) > 0 then
      if not DB.Users.EditPassword(FUserDetails.PartyID,FNewPassword) then
        Exit;

    if FOriginalUserDetails.IsActive <> FUserDetails.IsActive then
      if not DB.Users.SetIsActive(FUserDetails.PartyID,FUserDetails.IsActive) then Exit;

    if not IsSameList(FOriginalUserDetails.Roles,FUserDetails.Roles) then
    begin
      //look for removed Privileges
      for i := 0 to Length(FOriginalUserDetails.Roles) - 1 do
      begin
        if not FOriginalUserDetails.Roles[i].Find(FUserDetails.Roles) then
          if not DB.UserRoles.Delete(FUserDetails.PartyID, FOriginalUserDetails.Roles[i].ID) then
            Exit;
      end;
      //look for added Privileges
      for i := 0 to Length(FUserDetails.Roles) - 1 do
      begin
        if not FUserDetails.Roles[i].Find(FOriginalUserDetails.Roles) then
          if not DB.UserRoles.Insert(FUserDetails.PartyID,FUserDetails.Roles[i].ID) then
            Exit;
      end;
    end;

    Result := True;
  end;
begin
  case FMode of
    umNew: Result := Insert;
    umDuplicate: Result := Insert;
    umEdit: Result := Update;
  end;
end;

function TUsersInsertUpdateModel.SelectAllUsersAvailableRolesCreateArray(
  const AUserID: Integer): TRoles;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.RolesUserRoles.SelectAllUsersAvailableRolesCreateArray(AUserID);
end;

function TUsersInsertUpdateModel.SelectAllUsersRolesCreateArray(
  const AUserID: Integer): TRoles;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.RolesUserRoles.SelectAllUsersRolesCreateArray(AUserID);
end;

procedure TUsersInsertUpdateModel.SelfTest;
begin
  inherited;

end;

procedure TUsersInsertUpdateModel.SetGivenName(const Value: String);
begin
  FUserDetails.GivenName := Value;
end;

procedure TUsersInsertUpdateModel.SetIsActive(const Value: Boolean);
begin
  FUserDetails.IsActive := Value;
end;

procedure TUsersInsertUpdateModel.SetPrivileges(
  const AUsersPrivileges: TUsersPrivileges);
begin
  HasViewPrivileges := AUsersPrivileges.HasUsersView;
  HasInsertPrivileges := AUsersPrivileges.HasUsersInsert;
  HasUpdatePrivileges := AUsersPrivileges.HasUsersUpdate;
  HasDeletePrivileges := AUsersPrivileges.HasUsersDelete;
end;

procedure TUsersInsertUpdateModel.SetSurname(const Value: String);
begin
  FUserDetails.Surname := Value;
end;

procedure TUsersInsertUpdateModel.SetUsername(const Value: String);
begin
  FUserDetails.Username := Value;
end;

end.
