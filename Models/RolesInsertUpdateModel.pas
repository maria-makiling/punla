unit RolesInsertUpdateModel;

interface

uses
    BaseModel
  , ProjectTypes
  , Global
  ;

{$M+}

type
  TRolesInsertUpdateModel = class(TBaseModel)
    procedure DefaultSettings; override;
    procedure SelfTest; override;
    procedure SetPrivileges(const AUsersPrivileges: TUsersPrivileges);
    function SelectAllRolesAvailablePrivilegesCreateArray(
      const ARoleID: Integer): TPrivileges;
    function SelectAllRolesPrivilegesCreateArray(const ARoleID: Integer): TPrivileges;
  private
    FMode: TRolesInsertUpdateModes;
    FOriginalRoleDetails: TRoleDetails;
    FRoleDetails: TRoleDetails;
    FAvailablePrivileges: TPrivileges;
    procedure FreeAssignedPrivileges;
    procedure FreeAvailablePrivileges;
    procedure SetRoleName(const Value: String);
    procedure SetIsActive(const Value: Boolean);
  public
    destructor Destroy; override;
    procedure Initialize(const AMode: TRolesInsertUpdateModes;
      const ARoleDetails: TRoleDetails);
    procedure RefreshLists;
    function Save: Boolean;
    procedure AssignAllPrivileges;
    procedure AssignPrivilege(const AIndex: Integer);
    procedure RemoveAllPrivileges;
    procedure RemovePrivilege(const AIndex: Integer);
    function IsBtnAssignAllEnabled: Boolean;
    function IsBtnAssignEnabled(const AIndex: Integer): Boolean;
    function IsBtnRemoveEnabled(const AIndex: Integer): Boolean;
    function IsBtnRemoveAllEnabled: Boolean;
  published
    property RoleDetails: TRoleDetails read FRoleDetails;
    property AvailablePrivileges: TPrivileges read FAvailablePrivileges;
    property Rolename: String write SetRolename;
    property IsActive: Boolean write SetIsActive;
  end;

implementation

uses
    System.SysUtils
  , Utils
  ;

{ TRolesInsertUpdateModel }

procedure TRolesInsertUpdateModel.AssignAllPrivileges;
begin
  FRoleDetails.Privileges := FRoleDetails.Privileges + FAvailablePrivileges;
  FAvailablePrivileges := nil;
end;

procedure TRolesInsertUpdateModel.AssignPrivilege(const AIndex: Integer);
var
  i: Integer;
  newList: TPrivileges;
begin
  if AIndex > Length(FAvailablePrivileges) - 1 then Exit;

  for i := 0 to Length(FAvailablePrivileges) - 1 do
  begin
    if i = AIndex then
      FRoleDetails.Privileges := FRoleDetails.Privileges + [FAvailablePrivileges[i]]
    else
      newList := newList + [FAvailablePrivileges[i]];
  end;
  FAvailablePrivileges := nil;
  FAvailablePrivileges := newList;
end;

procedure TRolesInsertUpdateModel.DefaultSettings;
begin
  inherited;

end;

destructor TRolesInsertUpdateModel.Destroy;
begin
  FreeAssignedPrivileges;
  FreeAvailablePrivileges;
  inherited;
end;

procedure TRolesInsertUpdateModel.FreeAssignedPrivileges;
var
  i: Integer;
begin
  for i := Length(FRoleDetails.Privileges) - 1 downto 0 do
    if Assigned(FRoleDetails.Privileges[i]) then FreeAndNil(FRoleDetails.Privileges[i]);
  FRoleDetails.Privileges := nil;
end;

procedure TRolesInsertUpdateModel.FreeAvailablePrivileges;
var
  i: Integer;
begin
  for i := Length(FAvailablePrivileges) - 1 downto 0 do
    if Assigned(FAvailablePrivileges[i]) then FreeAndNil(FAvailablePrivileges[i]);
  FAvailablePrivileges := nil;
end;

procedure TRolesInsertUpdateModel.Initialize(
  const AMode: TRolesInsertUpdateModes; const ARoleDetails: TRoleDetails);
var
  i: Integer;
  Privileges: TPrivileges;
begin
  FreeAssignedPrivileges;
  FreeAvailablePrivileges;
  SetPrivileges(FGlobal.UserPrivileges);
  FMode := AMode;
  FOriginalRoleDetails := ARoleDetails;
  //default values
  FRoleDetails.ID := -1;
  FRoleDetails.Rolename := '';
  FRoleDetails.IsActive := True;
  case FMode of
    rmNew:
      begin
        FAvailablePrivileges := SelectAllRolesAvailablePrivilegesCreateArray(-1);
      end;
    rmDuplicate:
      begin
        Privileges := SelectAllRolesAvailablePrivilegesCreateArray(-1);
        for i := 0 to Length(Privileges) - 1 do
        begin
          if Privileges[i].Find(ARoleDetails.Privileges) then
            FRoleDetails.Privileges := FRoleDetails.Privileges + [Privileges[i]]
          else
            FAvailablePrivileges := FAvailablePrivileges + [Privileges[i]];
        end;
      end;
    rmEdit:
      begin
        FRoleDetails.ID := ARoleDetails.ID;
        FRoleDetails.RoleName := ARoleDetails.RoleName;
        FRoleDetails.IsActive := ARoleDetails.IsActive;
        Privileges := SelectAllRolesAvailablePrivilegesCreateArray(-1);
        for i := 0 to Length(Privileges) - 1 do
        begin
          if Privileges[i].Find(ARoleDetails.Privileges) then
            FRoleDetails.Privileges := FRoleDetails.Privileges + [Privileges[i]]
          else
            FAvailablePrivileges := FAvailablePrivileges + [Privileges[i]];
        end;
      end;
  end;
end;

function TRolesInsertUpdateModel.IsBtnAssignAllEnabled: Boolean;
begin
  Result := Length(FAvailablePrivileges) > 0;
end;

function TRolesInsertUpdateModel.IsBtnAssignEnabled(
  const AIndex: Integer): Boolean;
begin
  Result := IsBtnAssignAllEnabled and (AIndex >= 0);
end;

function TRolesInsertUpdateModel.IsBtnRemoveAllEnabled: Boolean;
begin
  Result := Length(FRoleDetails.Privileges) > 0;
end;

function TRolesInsertUpdateModel.IsBtnRemoveEnabled(
  const AIndex: Integer): Boolean;
begin
  Result := IsBtnRemoveAllEnabled and (AIndex >= 0);
end;

//this should be able to handle sudden additional rows to Privileges table
procedure TRolesInsertUpdateModel.RefreshLists;
var
  i: Integer;
  Privileges: TPrivileges;
  NewAssignedPrivileges: TPrivileges;
begin
  FreeAvailablePrivileges;
  Privileges := SelectAllRolesAvailablePrivilegesCreateArray(-1); //get all Privileges
  for i := 0 to Length(Privileges) - 1 do
  begin
    if Privileges[i].Find(FRoleDetails.Privileges) then
      NewAssignedPrivileges := NewAssignedPrivileges + [Privileges[i]]
    else
      FAvailablePrivileges := FAvailablePrivileges + [Privileges[i]];
  end;
  FreeAssignedPrivileges;
  FRoleDetails.Privileges := NewAssignedPrivileges;
end;

procedure TRolesInsertUpdateModel.RemoveAllPrivileges;
begin
  FAvailablePrivileges := FAvailablePrivileges + FRoleDetails.Privileges;
  FRoleDetails.Privileges := nil;
end;

procedure TRolesInsertUpdateModel.RemovePrivilege(const AIndex: Integer);
var
  i: Integer;
  newList: TPrivileges;
begin
  if AIndex > Length(FRoleDetails.Privileges) - 1 then Exit;

  for i := 0 to Length(FRoleDetails.Privileges) - 1 do
  begin
    if i = AIndex then
      FAvailablePrivileges := FAvailablePrivileges + [FRoleDetails.Privileges[i]]
    else
      newList := newList + [FRoleDetails.Privileges[i]];
  end;
  FRoleDetails.Privileges := nil;
  FRoleDetails.Privileges := newList;
end;

function TRolesInsertUpdateModel.Save: Boolean;
  function IsRolenameUnique(const ASearch: String): Boolean;
  begin
    Result := False;
    if not HasViewPrivileges then exit;

    Result := DB.Roles.IsRolenameUnique(ASearch);
  end;

  function IsRequiredFieldsValid: Boolean;
  var
    Msg: String;
  begin
    Result := True;
    if Length(FRoleDetails.RoleName) = 0 then
    begin
      Result := False;
      Msg := Msg + 'Role name can''t be blank.' + sLineBreak;
    end;
    if (not IsRolenameUnique(FRoleDetails.RoleName) and
      (FRoleDetails.RoleName <> FOriginalRoleDetails.RoleName)) then
    begin
      Result := False;
      Msg := Msg + 'Rolename must be unique.' + sLineBreak;
    end;

    if not Result then
      Utils.MsgWarning(Msg);
  end;

  function Insert: Boolean;
  var
    i, NewRoleID: Integer;
  begin
    Result := False;
    if not IsRequiredFieldsValid then Exit;

    NewRoleID := DB.Roles.Insert(FRoleDetails.RoleName);
    if NewRoleID < 0 then
      Exit;
    if not DB.Roles.SetIsActive(NewRoleID, FRoleDetails.IsActive) then
      Exit;

    for i := 0 to Length(FRoleDetails.Privileges) - 1 do
    begin
      if not DB.RolePrivileges.Insert(
        NewRoleID,
        FRoleDetails.Privileges[i].ID
      ) then
        Exit;
    end;

    Result := True;
  end;

  function Update: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if not IsRequiredFieldsValid then Exit;
    if FRoleDetails.ID <= 0 then Exit;

    if FOriginalRoleDetails.RoleName <> FRoleDetails.RoleName then
      if not DB.Roles.Edit(FRoleDetails.ID, FRoleDetails.RoleName) then Exit;

    if FOriginalRoleDetails.IsActive <> FRoleDetails.IsActive then
      if not DB.Roles.SetIsActive(FRoleDetails.ID, FRoleDetails.IsActive) then
        Exit;

    if not IsSameList(
      FOriginalRoleDetails.Privileges,
      FRoleDetails.Privileges) then
    begin
      //look for removed Privileges
      for i := 0 to Length(FOriginalRoleDetails.Privileges) - 1 do
      begin
        if not FOriginalRoleDetails.Privileges[i].Find(FRoleDetails.Privileges) then
          if not DB.RolePrivileges.Delete(FRoleDetails.ID, FOriginalRoleDetails.Privileges[i].ID) then
            Exit;
      end;
      //look for added Privileges
      for i := 0 to Length(FRoleDetails.Privileges) - 1 do
      begin
        if not FRoleDetails.Privileges[i].Find(FOriginalRoleDetails.Privileges) then
          if not DB.RolePrivileges.Insert(FRoleDetails.ID,FRoleDetails.Privileges[i].ID) then
            Exit;
      end;
    end;

    Result := True;
  end;
begin
  case FMode of
    rmNew: Result := Insert;
    rmDuplicate: Result := Insert;
    rmEdit: Result := Update;
  end;
end;

function TRolesInsertUpdateModel.SelectAllRolesAvailablePrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.PrivilegesRolePrivileges.SelectAllRolesAvailablePrivilegesCreateArray(ARoleID);
end;

function TRolesInsertUpdateModel.SelectAllRolesPrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.PrivilegesRolePrivileges.SelectAllRolesPrivilegesCreateArray(ARoleID);
end;

procedure TRolesInsertUpdateModel.SelfTest;
begin
  inherited;

end;

procedure TRolesInsertUpdateModel.SetIsActive(const Value: Boolean);
begin
  FRoleDetails.IsActive := Value;
end;

procedure TRolesInsertUpdateModel.SetPrivileges(
  const AUsersPrivileges: TUsersPrivileges);
begin
  HasViewPrivileges := AUsersPrivileges.HasRolesView;
  HasInsertPrivileges := AUsersPrivileges.HasRolesInsert;
  HasUpdatePrivileges := AUsersPrivileges.HasRolesUpdate;
  HasDeletePrivileges := AUsersPrivileges.HasRolesDelete;
end;

procedure TRolesInsertUpdateModel.SetRoleName(const Value: String);
begin
  FRoleDetails.RoleName := Value;
end;

end.
