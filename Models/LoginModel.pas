unit LoginModel;

interface

uses
    BaseModel
  , Database
  , Global
  , System.SysUtils
  ;

{$M+}

type
  TLoginModel = class(TBaseModel)
    procedure SelfTest; override;
  private
  public
    function DoTryConnect: Boolean;
    function DoTryLogin(const AUsername: String; const APassword: String): Boolean;
  end;

implementation

{ TLoginModel }

function TLoginModel.DoTryConnect: Boolean;
begin
  Result := DB.TryConnect;
  if Result and DB.Versions.FetchVersion then
  begin
    Global.Version.DBMajorVersion := DB.Versions.DBMajorVersion;
    Global.Version.DBMinorVersion := DB.Versions.DBMinorVersion;
    Global.Version.AppMajorVersion := DB.Versions.AppMajorVersion;
    Global.Version.AppMinorVersion := DB.Versions.AppMinorVersion;
    Global.Version.AppBuildVersion := DB.Versions.AppBuildVersion;
    Global.Version.AppRevisionVersion := DB.Versions.AppRevisionVersion;
  end;
end;

function TLoginModel.DoTryLogin(const AUsername, APassword: String): Boolean;
var
  UserID: Integer;
begin
  Result := False;
  if not DoTryConnect then Exit;

  UserID := DB.Users.GetPartyID(AUsername, APassword);
  if (UserID > 0) then
  begin
    Global.Status.UserID := UserID;
    Global.Status.Username := AUsername;
    Result := True;
  end;
end;

procedure TLoginModel.SelfTest;
begin
  Assert(DoTryConnect, 'test default connection settings');
  Assert(DoTryLogin(Global.Status.Username,Global.Status.Username), 'test default connection settings');
end;

end.
