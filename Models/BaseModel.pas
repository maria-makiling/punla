unit BaseModel;

interface

uses
    Database
  , Global
  ;


{$M+}

type
  TBaseModel = class
    FDB: TDatabase;
    FGlobal: TGlobal;
    procedure DefaultSettings; virtual;
    procedure SelfTest; virtual;
  private
    FHasViewPrivileges: Boolean;
    FHasInsertPrivileges: Boolean;
    FHasUpdatePrivileges: Boolean;
    FHasDeletePrivileges: Boolean;
    function GetViewPrivileges: Boolean;
    function GetInsertPrivileges: Boolean;
    function GetUpdatePrivileges: Boolean;
    function GetDeletePrivileges: Boolean;
    procedure SetViewPrivileges(const Value: Boolean);
    procedure SetInsertPrivileges(const Value: Boolean);
    procedure SetUpdatePrivileges(const Value: Boolean);
    procedure SetDeletePrivileges(const Value: Boolean);
    procedure SetPrivileges(const AUsersPrivileges: TUsersPrivileges); virtual;
  public
    constructor Create(const ADatabase: TDatabase; AGlobal: TGlobal);
  published
    property DB: TDatabase read FDB;
    property Global: TGlobal read FGlobal;
    property HasViewPrivileges: Boolean read GetViewPrivileges write SetViewPrivileges;
    property HasInsertPrivileges: Boolean read GetInsertPrivileges write SetInsertPrivileges;
    property HasUpdatePrivileges: Boolean read GetUpdatePrivileges write SetUpdatePrivileges;
    property HasDeletePrivileges: Boolean read GetDeletePrivileges write SetDeletePrivileges;
  end;

implementation

uses
    Constants
  ;

{ TBaseModel }

constructor TBaseModel.Create(const ADatabase: TDatabase; AGlobal: TGlobal);
begin
  FDB := ADatabase;
  FGlobal := AGlobal;
  DefaultSettings;
  if RUN_SELF_TEST then SelfTest;
end;

procedure TBaseModel.DefaultSettings;
begin
  FHasViewPrivileges := False;
  FHasInsertPrivileges := False;
  FHasUpdatePrivileges := False;
  FHasDeletePrivileges := False;
end;

function TBaseModel.GetDeletePrivileges: Boolean;
begin
  Result := FHasDeletePrivileges;
end;

function TBaseModel.GetInsertPrivileges: Boolean;
begin
  Result := FHasInsertPrivileges;
end;

function TBaseModel.GetUpdatePrivileges: Boolean;
begin
  Result := FHasUpdatePrivileges;
end;

function TBaseModel.GetViewPrivileges: Boolean;
begin
  Result := FHasViewPrivileges;
end;

procedure TBaseModel.SelfTest;
begin
  DefaultSettings;
end;

procedure TBaseModel.SetDeletePrivileges(const Value: Boolean);
begin
  FHasDeletePrivileges := Value;
end;

procedure TBaseModel.SetInsertPrivileges(const Value: Boolean);
begin
  FHasInsertPrivileges := Value;
end;

procedure TBaseModel.SetPrivileges(const AUsersPrivileges: TUsersPrivileges);
begin
  FHasViewPrivileges := False;
  FHasInsertPrivileges := False;
  FHasUpdatePrivileges := False;
  FHasDeletePrivileges := False;
end;

procedure TBaseModel.SetUpdatePrivileges(const Value: Boolean);
begin
  FHasUpdatePrivileges := Value;
end;

procedure TBaseModel.SetViewPrivileges(const Value: Boolean);
begin
  FHasViewPrivileges := Value;
end;

end.
