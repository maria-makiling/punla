unit UsersViewModel;

interface

uses
    BaseModel
  , ProjectTypes
  , Global
  ;

{$M+}

type
  TUsersViewModel = class(TBaseModel)
    procedure DefaultSettings; override;
    procedure SelfTest; override;
  private
    FShowActiveOnly: Boolean;
    FPersons: TPersons;
    FUsers: TUsers;
    FAssignedRoles: TRoles;
    FAvailableRoles: TRoles;
    FSelectedUserID: Integer;
    procedure FreeAssignedRoles;
    procedure FreeAvailableRoles;
    procedure FreeUsers;
    procedure PopulateAssignedRolesList;
    procedure PopulateAvailableRolesList;
    procedure PopulateUserList;
    function SelectAllUsersCreateArray: TUsers;
    function SelectAllUsersAvailableRolesCreateArray(
      const AUserID: Integer): TRoles;
    function SelectAllUsersRolesCreateArray(const AUserID: Integer): TRoles;
    procedure SetPrivileges(const AUsersPrivileges: TUsersPrivileges);
    function GetUsersLabel: String;
    procedure SetShowActiveOnly(const Value: Boolean);
    function GetAssignedRolesLabel: String;
    function GetAvailableRolesLabel: String;
    procedure SetSelectedUserID(const Value: Integer);
  public
    destructor Destroy; override;
    procedure Initialize;
    function Delete(const APartyID: Integer): Boolean;
    function GetUserDetails(const AIndex: Integer): TUserDetails;
  published
    property Persons: TPersons read FPersons;
    property Users: TUsers read FUsers;
    property AssignedRoles: TRoles read FAssignedRoles;
    property AvailableRoles: TRoles read FAvailableRoles;
    property SelectedUserID: Integer read FSelectedUserID write SetSelectedUserID;
    property UsersLabel: String read GetUsersLabel;
    property AssignedRolesLabel: String read GetAssignedRolesLabel;
    property AvailableRolesLabel: String read GetAvailableRolesLabel;
    property ShowActiveOnly: Boolean read FShowActiveOnly write SetShowActiveOnly;
  end;

implementation

uses
    ZDataset
  , Constants
  , Utils
  , System.SysUtils
  ;

{ TUsersViewModel }

function TUsersViewModel.Delete(const APartyID: Integer): Boolean;
var
  i: Integer;
  Roles: TRoles;
begin
  Result := False;
  if not HasDeletePrivileges then exit;

  Roles := SelectAllUsersRolesCreateArray(APartyID);
  if Roles <> nil then
  begin
    for i := Length(Roles) - 1 downto 0 do
      DB.UserRoles.Delete(APartyID, Roles[i].ID);
    for i := Length(Roles) - 1 downto 0 do
      if Assigned(Roles[i]) then FreeAndNil(Roles[i]);
  end;

  Result := DB.Users.Delete(APartyID);
  if Result then
    Initialize;
end;

destructor TUsersViewModel.Destroy;
begin
  FreeAssignedRoles;
  FreeAvailableRoles;
  FreeUsers;
  inherited;
end;

procedure TUsersViewModel.FreeAssignedRoles;
var
  i: Integer;
begin
  for i := Length(FAssignedRoles) - 1 downto 0 do
    if Assigned(FAssignedRoles[i]) then FreeAndNil(FAssignedRoles[i]);
  FAssignedRoles := nil;
end;

procedure TUsersViewModel.FreeAvailableRoles;
var
  i: Integer;
begin
  for i := Length(FAvailableRoles) - 1 downto 0 do
    if Assigned(FAvailableRoles[i]) then FreeAndNil(FAvailableRoles[i]);
  FAvailableRoles := nil;
end;

procedure TUsersViewModel.FreeUsers;
var
  i: Integer;
begin
  for i := Length(FUsers) - 1 downto 0 do
    if Assigned(FUsers[i]) then FreeAndNil(FUsers[i]);
  for i := Length(FPersons) - 1 downto 0 do
    if Assigned(FPersons[i]) then FreeAndNil(FPersons[i]);
end;

function TUsersViewModel.GetAssignedRolesLabel: String;
begin
  Result := ListNameNum(LABEL_ASSIGNED_ROLES,Length(FAssignedRoles));
end;

function TUsersViewModel.GetAvailableRolesLabel: String;
begin
  Result := ListNameNum(LABEL_AVAILABLE_ROLES,Length(FAvailableRoles));
end;

function TUsersViewModel.GetUserDetails(const AIndex: Integer): TUserDetails;
begin
  if AIndex < 0 then Exit;
  if AIndex > Length(FUsers) - 1 then Exit;
  if AIndex > Length(FPersons) - 1 then Exit;

  Result.PartyID := FUsers[AIndex].PartyID;
  Result.GivenName := FPersons[AIndex].GivenName;
  Result.Surname := FPersons[AIndex].Surname;
  Result.Username := FUsers[AIndex].Username;
  Result.IsActive := FUsers[AIndex].IsActive;
  Result.Roles := FAssignedRoles; //it is assumed that AIndex correspond to the currently selected user
end;

function TUsersViewModel.GetUsersLabel: String;
begin
  Result := ListActiveNameNum(FShowActiveOnly,LABEL_USERS,Length(FUsers));
end;

procedure TUsersViewModel.DefaultSettings;
begin
  FShowActiveOnly := True;
  HasViewPrivileges := False;
  HasInsertPrivileges := False;
  HasUpdatePrivileges := False;
  HasDeletePrivileges := False;
end;

procedure TUsersViewModel.Initialize;
begin
  SetPrivileges(FGlobal.UserPrivileges);
  PopulateUserList;
  PopulateAssignedRolesList;
  PopulateAvailableRolesList;
end;

procedure TUsersViewModel.PopulateAssignedRolesList;
begin
  FreeAssignedRoles;
  FAssignedRoles := SelectAllUsersRolesCreateArray(FSelectedUserID);
end;

procedure TUsersViewModel.PopulateAvailableRolesList;
begin
  FreeAvailableRoles;
  FAvailableRoles := SelectAllUsersAvailableRolesCreateArray(FSelectedUserID);
end;

procedure TUsersViewModel.PopulateUserList;
var
  i: Integer;
begin
  FreeUsers;
  FUsers := SelectAllUsersCreateArray;
  SetLength(FPersons, Length(FUsers));
  for i := 0 to Length(FUsers) - 1 do
  begin
    FPersons[i] := DB.Persons.GetPerson(FUsers[i].PartyID);
  end;
end;

function TUsersViewModel.SelectAllUsersAvailableRolesCreateArray(
  const AUserID: Integer): TRoles;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.RolesUserRoles.SelectAllUsersAvailableRolesCreateArray(AUserID);
end;

function TUsersViewModel.SelectAllUsersCreateArray: TUsers;
var
  SQL: TZQuery;
  User: TUser;
  i: Integer;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  if FShowActiveOnly then
    SQL := DB.Users.SelectAllActiveCreateQry
  else
    SQL := DB.Users.SelectAllCreateQry;

  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    User := TUser.Create(
      SQL.FieldByName(USERS_DB_PARTY_ID).AsInteger,
      SQL.FieldByName(USERS_DB_USERNAME).AsString,
      SQL.FieldByName(USERS_DB_PASSWORD).AsString,
      Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger)
    );
    Result[i] := User;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TUsersViewModel.SelectAllUsersRolesCreateArray(
  const AUserID: Integer): TRoles;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  Result := DB.RolesUserRoles.SelectAllUsersRolesCreateArray(AUserID);
end;

procedure TUsersViewModel.SelfTest;
begin

end;

procedure TUsersViewModel.SetPrivileges(
  const AUsersPrivileges: TUsersPrivileges);
begin
  HasViewPrivileges := AUsersPrivileges.HasUsersView;
  HasInsertPrivileges := AUsersPrivileges.HasUsersInsert;
  HasUpdatePrivileges := AUsersPrivileges.HasUsersUpdate;
  HasDeletePrivileges := AUsersPrivileges.HasUsersDelete;
end;

procedure TUsersViewModel.SetSelectedUserID(const Value: Integer);
begin
  FSelectedUserID := Value;
  Initialize;
end;

procedure TUsersViewModel.SetShowActiveOnly(const Value: Boolean);
begin
  FShowActiveOnly := Value;
  Initialize;
end;

end.
