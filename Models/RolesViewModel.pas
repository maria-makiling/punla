unit RolesViewModel;

interface

uses
    BaseModel
  , ProjectTypes
  , Global
  ;

{$M+}

type
  TRolesViewModel = class(TBaseModel)
    procedure DefaultSettings; override;
    procedure SelfTest; override;
  private
    FShowActiveOnly: Boolean;
    FRoles: TRoles;
    FAssignedPrivileges: TPrivileges;
    FAvailablePrivileges: TPrivileges;
    FSelectedRoleID: Integer;
    procedure FreeAssignedPrivileges;
    procedure FreeAvailablePrivileges;
    procedure FreeRoles;
    procedure PopulateAssignedPrivilegesList;
    procedure PopulateAvailablePrivilegesList;
    procedure PopulateRoleList;
    function SelectAllRolesCreateArray: TRoles;
    function SelectAllRolesAvailablePrivilegesCreateArray(
      const ARoleID: Integer): TPrivileges;
    function SelectAllRolesPrivilegesCreateArray(const ARoleID: Integer): TPrivileges;
    function SelectAllRolesUsersCreateArray(const ARoleID: Integer): TUsers;
    procedure SetPrivileges(const AUsersPrivileges: TUsersPrivileges);
    function GetRolesLabel: String;
    procedure SetShowActiveOnly(const Value: Boolean);
    function GetAssignedPrivilegesLabel: String;
    function GetAvailablePrivilegesLabel: String;
    procedure SetSelectedRoleID(const Value: Integer);
  public
    destructor Destroy; override;
    procedure Initialize;
    function Delete(const ARoleID: Integer): Boolean;
    function GetRoleDetails(const AIndex: Integer): TRoleDetails;
    function GetSelectedRoleIndex: Integer;
  published
    property Roles: TRoles read FRoles;
    property AssignedPrivileges: TPrivileges read FAssignedPrivileges;
    property AvailablePrivileges: TPrivileges read FAvailablePrivileges;
    property SelectedRoleID: Integer read FSelectedRoleID write SetSelectedRoleID;
    property RolesLabel: String read GetRolesLabel;
    property AssignedPrivilegesLabel: String read GetAssignedPrivilegesLabel;
    property AvailablePrivilegesLabel: String read GetAvailablePrivilegesLabel;
    property ShowActiveOnly: Boolean read FShowActiveOnly write SetShowActiveOnly;
  end;

implementation

uses
    ZDataset
  , Constants
  , Utils
  , System.SysUtils
  ;

{ TRolesViewModel }

function TRolesViewModel.Delete(const ARoleID: Integer): Boolean;
var
  i: Integer;
  Users: TUsers;
  Privileges: TPrivileges;
begin
  Result := False;
  if not HasDeletePrivileges then exit;

  Users := SelectAllRolesUsersCreateArray(ARoleID);
  if Users <> nil then
  begin
    for i := Length(Users) - 1 downto 0 do
      DB.UserRoles.Delete(Users[i].PartyID, ARoleID);
    for i := Length(Users) - 1 downto 0 do
      if Assigned(Users[i]) then FreeAndNil(Users[i]);
  end;

  Privileges := SelectAllRolesPrivilegesCreateArray(ARoleID);
  if Privileges <> nil then
  begin
    for i := Length(Privileges) - 1 downto 0 do
      DB.RolePrivileges.Delete(ARoleID, Privileges[i].ID);
    for i := Length(Privileges) - 1 downto 0 do
      if Assigned(Privileges[i]) then FreeAndNil(Privileges[i]);
  end;

  Result := DB.Roles.Delete(ARoleID);
  if Result then
    Initialize;
end;

destructor TRolesViewModel.Destroy;
begin
  FreeAssignedPrivileges;
  FreeAvailablePrivileges;
  FreeRoles;
  inherited;
end;

procedure TRolesViewModel.FreeAssignedPrivileges;
var
  i: Integer;
begin
  for i := Length(FAssignedPrivileges) - 1 downto 0 do
    if Assigned(FAssignedPrivileges[i]) then FreeAndNil(FAssignedPrivileges[i]);
  FAssignedPrivileges := nil;
end;

procedure TRolesViewModel.FreeAvailablePrivileges;
var
  i: Integer;
begin
  for i := Length(FAvailablePrivileges) - 1 downto 0 do
    if Assigned(FAvailablePrivileges[i]) then FreeAndNil(FAvailablePrivileges[i]);
  FAvailablePrivileges := nil;
end;

procedure TRolesViewModel.FreeRoles;
var
  i: Integer;
begin
  for i := Length(FRoles) - 1 downto 0 do
    if Assigned(FRoles[i]) then FreeAndNil(FRoles[i]);
end;

function TRolesViewModel.GetAssignedPrivilegesLabel: String;
begin
  Result := ListNameNum(LABEL_ASSIGNED_PRIVILEGES,Length(FAssignedPrivileges));
end;

function TRolesViewModel.GetAvailablePrivilegesLabel: String;
begin
  Result := ListNameNum(LABEL_AVAILABLE_PRIVILEGES,Length(FAvailablePrivileges));
end;

function TRolesViewModel.GetRoleDetails(const AIndex: Integer): TRoleDetails;
begin
  if AIndex < 0 then Exit;
  if AIndex > Length(FRoles) - 1 then Exit;

  Result.ID := FRoles[AIndex].ID;
  Result.RoleName := FRoles[AIndex].Name;
  Result.IsActive := FRoles[AIndex].IsActive;
  Result.Privileges := FAssignedPrivileges; //it is assumed that AIndex correspond to the currently selected Role
end;

function TRolesViewModel.GetRolesLabel: String;
begin
  Result := ListActiveNameNum(FShowActiveOnly,LABEL_ROLES,Length(FRoles));
end;

function TRolesViewModel.GetSelectedRoleIndex: Integer;
var
  i: Integer;
begin
  Result := -1;
  if Length(FRoles) <= 0 then Exit;

  Result := 0;
  if FSelectedRoleID <= 0 then Exit;

  for i := 0 to Length(FRoles) - 1 do
    if FSelectedRoleID = FRoles[i].ID then
    begin
      Result := i;
      Exit;
    end;
end;

procedure TRolesViewModel.DefaultSettings;
begin
  FShowActiveOnly := True;
  HasViewPrivileges := False;
  HasInsertPrivileges := False;
  HasUpdatePrivileges := False;
  HasDeletePrivileges := False;
end;

procedure TRolesViewModel.Initialize;
begin
  SetPrivileges(FGlobal.UserPrivileges);
  PopulateRoleList;
  PopulateAssignedPrivilegesList;
  PopulateAvailablePrivilegesList;
end;

procedure TRolesViewModel.PopulateAssignedPrivilegesList;
begin
  FreeAssignedPrivileges;
  FAssignedPrivileges := SelectAllRolesPrivilegesCreateArray(FSelectedRoleID);
end;

procedure TRolesViewModel.PopulateAvailablePrivilegesList;
begin
  FreeAvailablePrivileges;
  FAvailablePrivileges := SelectAllRolesAvailablePrivilegesCreateArray(FSelectedRoleID);
end;

procedure TRolesViewModel.PopulateRoleList;
var
  i: Integer;
begin
  FreeRoles;
  FRoles := SelectAllRolesCreateArray;
end;

function TRolesViewModel.SelectAllRolesAvailablePrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
var
  SQL: TZQuery;
  Privilege: TPrivilege;
  i: Integer;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  SQL := DB.PrivilegesRolePrivileges.GetRolesAvailablePrivileges(ARoleID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Privilege := TPrivilege.Create(
      SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger,
      SQL.FieldByName(PRIVILEGES_DB_NAME).AsString,
      SQL.FieldByName(PRIVILEGES_DB_DESCRIPTION).AsString
    );
    Result[i] := Privilege;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TRolesViewModel.SelectAllRolesCreateArray: TRoles;
var
  SQL: TZQuery;
  Role: TRole;
  i: Integer;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  if FShowActiveOnly then
    SQL := DB.Roles.SelectAllActiveCreateQry
  else
    SQL := DB.Roles.SelectAllCreateQry;

  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Role := TRole.Create(
      SQL.FieldByName(ROLES_DB_ID).AsInteger,
      SQL.FieldByName(ROLES_DB_NAME).AsString,
      Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger)
    );
    Result[i] := Role;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TRolesViewModel.SelectAllRolesPrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
var
  SQL: TZQuery;
  Privilege: TPrivilege;
  i: Integer;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  SQL := DB.PrivilegesRolePrivileges.GetRolesPrivileges(ARoleID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Privilege := TPrivilege.Create(
      SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger,
      SQL.FieldByName(PRIVILEGES_DB_NAME).AsString,
      SQL.FieldByName(PRIVILEGES_DB_DESCRIPTION).AsString
    );
    Result[i] := Privilege;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TRolesViewModel.SelectAllRolesUsersCreateArray(
  const ARoleID: Integer): TUsers;
var
  SQL: TZQuery;
  User: TUser;
  i: Integer;
begin
  Result := nil;
  if not HasViewPrivileges then exit;

  SQL := DB.RolesUserRoles.GetRolesUsers(ARoleID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    User := TUser.Create(
      SQL.FieldByName(USERS_DB_PARTY_ID).AsInteger,
      SQL.FieldByName(USERS_DB_USERNAME).AsString,
      SQL.FieldByName(USERS_DB_PASSWORD).AsString,
      Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger)
    );
    Result[i] := User;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

procedure TRolesViewModel.SelfTest;
begin

end;

procedure TRolesViewModel.SetPrivileges(
  const AUsersPrivileges: TUsersPrivileges);
begin
  HasViewPrivileges := AUsersPrivileges.HasRolesView;
  HasInsertPrivileges := AUsersPrivileges.HasRolesInsert;
  HasUpdatePrivileges := AUsersPrivileges.HasRolesUpdate;
  HasDeletePrivileges := AUsersPrivileges.HasRolesDelete;
end;

procedure TRolesViewModel.SetSelectedRoleID(const Value: Integer);
begin
  FSelectedRoleID := Value;
  Initialize;
end;

procedure TRolesViewModel.SetShowActiveOnly(const Value: Boolean);
begin
  FShowActiveOnly := Value;
  Initialize;
end;

end.
