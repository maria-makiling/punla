unit Models;

interface

uses
    System.SysUtils
  , Global
  , Database
  , LoginModel
  , UsersPrivilegesModel
  , UsersViewModel
  , UsersInsertUpdateModel
  , RolesViewModel
  , RolesInsertUpdateModel
  ;

{$M+}

type
  TModels = class
  private
    FDB: TDatabase;
    FGlobal: TGlobal;
    FLogin: TLoginModel;
    FUsersPrivileges: TUsersPrivilegesModel;
    FUsersView: TUsersViewModel;
    FUsersInsertUpdateModel: TUsersInsertUpdateModel;
    FRolesViewModel: TRolesViewModel;
    FRolesInsertUpdateModel: TRolesInsertUpdateModel;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Global: TGlobal read FGlobal;
    property Login: TLoginModel read FLogin;
    property UsersPrivileges: TUsersPrivilegesModel read FUsersPrivileges;
    property UsersView: TUsersViewModel read FUsersView;
    property UsersInsertUpdate: TUsersInsertUpdateModel read FUsersInsertUpdateModel;
    property RolesView: TRolesViewModel read FRolesViewModel;
    property RolesInsertUpdate: TRolesInsertUpdateModel read FRolesInsertUpdateModel;
  end;

implementation

{ TModels }

constructor TModels.Create;
begin
  FGlobal := TGlobal.Create;
  FDB := TDatabase.Create;
  FLogin := TLoginModel.Create(FDB, FGlobal);
  FUsersPrivileges := TUsersPrivilegesModel.Create(FDB, FGlobal);
  FUsersView := TUsersViewModel.Create(FDB, FGlobal);
  FUsersInsertUpdateModel := TUsersInsertUpdateModel.Create(FDB, FGlobal);
  FRolesViewModel := TRolesViewModel.Create(FDB, FGlobal);
  FRolesInsertUpdateModel := TRolesInsertUpdateModel.Create(FDB, FGlobal);
end;

destructor TModels.Destroy;
begin
  if Assigned(FRolesInsertUpdateModel) then FreeAndNil(FRolesInsertUpdateModel);
  if Assigned(FRolesViewModel) then FreeAndNil(FRolesViewModel);
  if Assigned(FUsersInsertUpdateModel) then FreeAndNil(FUsersInsertUpdateModel);
  if Assigned(FUsersView) then FreeAndNil(FUsersView);
  if Assigned(FUsersPrivileges) then FreeAndNil(FUsersPrivileges);
  if Assigned(FLogin) then FreeAndNil(FLogin);
  if Assigned(FDB) then FreeAndNil(FDB);
  if Assigned(FGlobal) then FreeAndNil(FGlobal);
  inherited;
end;

end.
