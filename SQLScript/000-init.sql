CREATE DATABASE IF NOT EXISTS punladb;
ALTER USER 'punla'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ASeedIsAForestInsideOut';
GRANT ALL ON punladb.* TO 'punla'@'localhost';
FLUSH PRIVILEGES;

USE punladb;

CREATE TABLE `parties` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `parties_pk` (`id`)
);

CREATE TABLE `organizations` (
  `party_id` INT NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',  
  PRIMARY KEY (`party_id`),
  CONSTRAINT `organizations_fk_parties`
    FOREIGN KEY (`party_id`)
    REFERENCES `parties` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);

CREATE TABLE `persons` (
  `party_id` INT NOT NULL,
  `given_name` varchar(50) NOT NULL DEFAULT '',
  `surname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`party_id`),
  CONSTRAINT `persons_fk_parties`
    FOREIGN KEY (`party_id`)
    REFERENCES `parties` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);

CREATE TABLE `users` (
  `party_id` int NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_active` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`party_id`),
  UNIQUE KEY `party_id_UNIQUE` (`party_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  CONSTRAINT `users_fk_parties` FOREIGN KEY (`party_id`) REFERENCES `parties` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO `parties` () VALUES ();
INSERT INTO `persons` (`party_id`, `given_name`, `surname`) VALUES ('1', 'Punla', 'Administrator');
INSERT INTO `users` (`party_id`, `username`, `password`) VALUES ('1', 'admin', '$2a$11$4BO7Fo2S9Gy7eW6A5dld.Oz6qUWcVyUY2CnaIj7b5MazXI94c0G0O');

CREATE TABLE `versions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `database_major` int NOT NULL,
  `database_minor` int NOT NULL,
  `application_major` int NOT NULL,
  `application_minor` int NOT NULL,
  `application_build` int NOT NULL,
  `application_revision` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(`id`)
);

INSERT INTO `versions` (`database_major`, `database_minor`, `application_major`, `application_minor`, `application_build`, `application_revision`) VALUES ('0', '1', '0', '1', '0', '0');

CREATE TABLE `roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `is_active` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

CREATE TABLE `privileges` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

CREATE TABLE `user_roles` (
  `user_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `user_roles_fk_role_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `user_roles_fk_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`party_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `user_roles_fk_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);
    
CREATE TABLE `role_privileges` (
  `role_id` INT NOT NULL,
  `privilege_id` INT NOT NULL,
  PRIMARY KEY (`role_id`, `privilege_id`),
  INDEX `role_privilege_fk_privilege_idx` (`privilege_id` ASC) VISIBLE,
  CONSTRAINT `role_privilege_fk_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `role_privilege_fk_privilege`
    FOREIGN KEY (`privilege_id`)
    REFERENCES `privileges` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);

INSERT INTO privileges (name, description) VALUES
  ('settings_view', 'Allows users to view general program settings'),
  ('settings_update', 'Allows users to change general program settings'),
  ('users_view', 'Allows users to view list of program users'),
  ('users_insert', 'Allows users to add new program users'),  
  ('users_update', 'Allows users to edit details and roles of other program users'),
  ('users_delete', 'Allows users to delete/deactivate existing program users'),
  ('roles_view', 'Allows users to view list of program user roles'),
  ('roles_insert', 'Allows users to add new program user roles'),
  ('roles_update', 'Allows users to edit existing program user roles'),
  ('roles_delete', 'Allows users to delete/deactivate existing program user roles');

INSERT INTO `roles` (`name`) VALUES ('Superuser');
INSERT INTO `role_privileges` (`role_id`, `privilege_id`)
  SELECT '1', `id` FROM privileges ORDER BY `id`;
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES ('1', '1');
