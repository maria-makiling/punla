object RolesInsertUpdate: TRolesInsertUpdate
  Left = 0
  Top = 0
  Caption = 'Roles'
  ClientHeight = 484
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 0
    object pnlDetails: TPanel
      Left = 2
      Top = 2
      Width = 475
      Height = 444
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 1
      TabOrder = 0
      object pnlProperties: TPanel
        Left = 1
        Top = 1
        Width = 473
        Height = 44
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        BorderWidth = 2
        TabOrder = 0
        object pnlIsActive: TPanel
          Left = 2
          Top = 22
          Width = 469
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object lblIsActive: TLabel
            Left = 2
            Top = 2
            Width = 100
            Height = 16
            Align = alLeft
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Active : '
            Layout = tlCenter
          end
          object cbIsActive: TCheckBox
            Left = 102
            Top = 2
            Width = 365
            Height = 16
            Align = alClient
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = cbIsActiveClick
          end
        end
        object pnlRoleName: TPanel
          Left = 2
          Top = 2
          Width = 469
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 0
          object lblRoleName: TLabel
            Left = 2
            Top = 2
            Width = 100
            Height = 16
            Align = alLeft
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Role name : '
            Layout = tlCenter
          end
          object edtRoleName: TEdit
            Left = 102
            Top = 2
            Width = 365
            Height = 16
            Align = alClient
            MaxLength = 50
            TabOrder = 0
            TextHint = 'Enter role name'
            OnExit = edtRoleNameExit
            ExplicitHeight = 21
          end
        end
      end
      object pnlPrivileges: TPanel
        Left = 1
        Top = 45
        Width = 473
        Height = 398
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 2
        Padding.Top = 6
        TabOrder = 1
        object pnlAssignedPrivileges: TPanel
          Left = 2
          Top = 8
          Width = 204
          Height = 388
          Align = alLeft
          Anchors = [akLeft, akTop, akRight, akBottom]
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object lblAssignedPrivileges: TLabel
            Left = 2
            Top = 2
            Width = 200
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Assigned Privileges (X)'
            ExplicitWidth = 108
          end
          object listAssignedPrivileges: TListBox
            Left = 2
            Top = 15
            Width = 200
            Height = 371
            Align = alClient
            BorderStyle = bsNone
            ItemHeight = 13
            TabOrder = 0
            OnClick = listAssignedPrivilegesClick
            OnDblClick = listAssignedPrivilegesDblClick
            ExplicitTop = 14
          end
        end
        object pnlAvailablePrivileges: TPanel
          Left = 267
          Top = 8
          Width = 204
          Height = 388
          Align = alRight
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 2
          object lblAvailablePrivileges: TLabel
            Left = 2
            Top = 2
            Width = 200
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Available Privileges (X)'
            ExplicitWidth = 108
          end
          object listAvailablePrivileges: TListBox
            Left = 2
            Top = 15
            Width = 200
            Height = 371
            Align = alClient
            BorderStyle = bsNone
            ItemHeight = 13
            TabOrder = 0
            OnClick = listAvailablePrivilegesClick
            OnDblClick = listAvailablePrivilegesDblClick
          end
        end
        object pnlModify: TPanel
          Left = 206
          Top = 8
          Width = 61
          Height = 388
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 0
          object pnlModifyButtons: TPanel
            Left = 2
            Top = 2
            Width = 57
            Height = 119
            Align = alTop
            BevelOuter = bvNone
            BorderWidth = 2
            Padding.Top = 10
            TabOrder = 0
            object btnRemoveAll: TButton
              Left = 2
              Top = 87
              Width = 53
              Height = 25
              Align = alTop
              Caption = '>>'
              TabOrder = 3
              OnClick = btnRemoveAllClick
            end
            object btnAssignAll: TButton
              Left = 2
              Top = 12
              Width = 53
              Height = 25
              Align = alTop
              Caption = '<<'
              TabOrder = 0
              OnClick = btnAssignAllClick
            end
            object btnAssign: TButton
              Left = 2
              Top = 37
              Width = 53
              Height = 25
              Align = alTop
              Caption = '<'
              TabOrder = 1
              OnClick = btnAssignClick
            end
            object btnRemove: TButton
              Left = 2
              Top = 62
              Width = 53
              Height = 25
              Align = alTop
              Caption = '>'
              TabOrder = 2
              OnClick = btnRemoveClick
            end
          end
        end
      end
    end
    object pnlControl: TPanel
      Left = 2
      Top = 446
      Width = 475
      Height = 36
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 6
      TabOrder = 1
      object btnOk: TButton
        Left = 394
        Top = 6
        Width = 75
        Height = 24
        Align = alRight
        Caption = '&OK'
        ModalResult = 1
        TabOrder = 0
        OnClick = btnOkClick
        ExplicitLeft = 395
      end
      object btnCancel: TButton
        Left = 6
        Top = 6
        Width = 75
        Height = 24
        Align = alLeft
        Caption = '&Cancel'
        ModalResult = 2
        TabOrder = 1
        OnClick = btnCancelClick
      end
    end
  end
end
