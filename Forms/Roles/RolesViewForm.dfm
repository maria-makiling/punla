object RolesView: TRolesView
  Left = 0
  Top = 0
  Caption = 'Roles'
  ClientHeight = 484
  ClientWidth = 612
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 612
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 0
    object pnlDetails: TPanel
      Left = 202
      Top = 2
      Width = 408
      Height = 444
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 1
      TabOrder = 1
      object pnlProperties: TPanel
        Left = 1
        Top = 1
        Width = 406
        Height = 44
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        BorderWidth = 2
        TabOrder = 0
        object pnlRoleName: TPanel
          Left = 2
          Top = 2
          Width = 402
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 0
          object lblRoleName: TLabel
            Left = 2
            Top = 2
            Width = 70
            Height = 16
            Align = alLeft
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Role name : '
            Layout = tlCenter
            ExplicitLeft = 3
            ExplicitTop = 0
          end
          object lblRoleNameField: TLabel
            Left = 72
            Top = 2
            Width = 328
            Height = 16
            Align = alClient
            Color = clWindow
            ParentColor = False
            Transparent = False
            Layout = tlCenter
            ExplicitWidth = 3
            ExplicitHeight = 13
          end
        end
        object pnlIsActive: TPanel
          Left = 2
          Top = 22
          Width = 402
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object lblIsActive: TLabel
            Left = 2
            Top = 2
            Width = 70
            Height = 16
            Align = alLeft
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Status : '
            Layout = tlCenter
            ExplicitTop = 4
          end
          object lblIsActiveField: TLabel
            Left = 72
            Top = 2
            Width = 328
            Height = 16
            Align = alClient
            Color = clWindow
            ParentColor = False
            Transparent = False
            Layout = tlCenter
            ExplicitWidth = 3
            ExplicitHeight = 13
          end
        end
      end
      object pnlPrivileges: TPanel
        Left = 1
        Top = 45
        Width = 406
        Height = 398
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 2
        Padding.Top = 6
        TabOrder = 1
        object pnlAssignedPrivileges: TPanel
          Left = 2
          Top = 8
          Width = 202
          Height = 388
          Align = alLeft
          Anchors = [akLeft, akTop, akRight, akBottom]
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 0
          object lblAssignedPrivileges: TLabel
            Left = 2
            Top = 2
            Width = 198
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Assigned Privileges (X)'
            ExplicitWidth = 108
          end
          object listAssignedPrivileges: TListBox
            Left = 2
            Top = 15
            Width = 198
            Height = 371
            Align = alClient
            BorderStyle = bsNone
            ItemHeight = 13
            TabOrder = 0
          end
        end
        object pnlAvailablePrivileges: TPanel
          Left = 204
          Top = 8
          Width = 200
          Height = 388
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          object lblAvailablePrivileges: TLabel
            Left = 2
            Top = 2
            Width = 196
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Available Privileges (X)'
            ExplicitWidth = 108
          end
          object listAvailablePrivileges: TListBox
            Left = 2
            Top = 15
            Width = 196
            Height = 371
            Align = alClient
            BorderStyle = bsNone
            ItemHeight = 13
            TabOrder = 0
          end
        end
      end
    end
    object pnlControl: TPanel
      Left = 2
      Top = 446
      Width = 608
      Height = 36
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 6
      TabOrder = 2
      object pnlSettings: TPanel
        Left = 233
        Top = 6
        Width = 215
        Height = 24
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object cbActiveOnly: TCheckBox
          Left = 58
          Top = 4
          Width = 105
          Height = 17
          Caption = 'Active Roles Only'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = cbActiveOnlyClick
        end
      end
      object pnlControlLeft: TPanel
        Left = 6
        Top = 6
        Width = 227
        Height = 24
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object btnNew: TButton
          Left = 0
          Top = 0
          Width = 75
          Height = 24
          Align = alLeft
          Caption = '&New'
          TabOrder = 0
          OnClick = btnNewClick
        end
        object btnDuplicate: TButton
          Left = 75
          Top = 0
          Width = 75
          Height = 24
          Align = alLeft
          Caption = 'D&uplicate'
          TabOrder = 1
          OnClick = btnDuplicateClick
        end
        object btnDelete: TButton
          Left = 150
          Top = 0
          Width = 75
          Height = 24
          Align = alLeft
          Caption = '&Delete'
          TabOrder = 2
          OnClick = btnDeleteClick
        end
      end
      object pnlControlRight: TPanel
        Left = 448
        Top = 6
        Width = 154
        Height = 24
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object btnOk: TButton
          Left = 79
          Top = 0
          Width = 75
          Height = 24
          Align = alRight
          Caption = '&OK'
          ModalResult = 1
          TabOrder = 0
          OnClick = btnOkClick
        end
        object btnEdit: TButton
          Left = 4
          Top = 0
          Width = 75
          Height = 24
          Align = alRight
          Caption = '&Edit'
          ModalResult = 1
          TabOrder = 1
          OnClick = btnEditClick
        end
      end
    end
    object pnlRoles: TPanel
      Left = 2
      Top = 2
      Width = 200
      Height = 444
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      BorderWidth = 6
      TabOrder = 0
      object lblRoles: TLabel
        Left = 6
        Top = 6
        Width = 188
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Roles (X)'
        ExplicitWidth = 43
      end
      object listRoles: TListBox
        Left = 6
        Top = 19
        Width = 188
        Height = 419
        Style = lbOwnerDrawFixed
        Align = alClient
        BorderStyle = bsNone
        ItemHeight = 13
        TabOrder = 0
        OnClick = listRolesClick
        OnDblClick = listRolesDblClick
        OnDrawItem = listRolesDrawItem
        OnEnter = listRolesEnter
      end
    end
  end
end
