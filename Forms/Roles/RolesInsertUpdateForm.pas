unit RolesInsertUpdateForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.StdCtrls
  , Vcl.ExtCtrls
  , Models
  , RolesInsertUpdateModel
  , RolesViewModel
  , ProjectTypes
  ;

type
  TRolesInsertUpdate = class(TForm)
    pnlMain: TPanel;
    pnlDetails: TPanel;
    pnlProperties: TPanel;
    pnlIsActive: TPanel;
    pnlRoleName: TPanel;
    pnlPrivileges: TPanel;
    pnlAssignedPrivileges: TPanel;
    pnlAvailablePrivileges: TPanel;
    pnlModify: TPanel;
    pnlModifyButtons: TPanel;
    pnlControl: TPanel;
    lblIsActive: TLabel;
    lblRoleName: TLabel;
    lblAssignedPrivileges: TLabel;
    lblAvailablePrivileges: TLabel;
    listAssignedPrivileges: TListBox;
    listAvailablePrivileges: TListBox;
    cbIsActive: TCheckBox;
    btnRemoveAll: TButton;
    btnAssignAll: TButton;
    btnAssign: TButton;
    btnRemove: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    edtRoleName: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtRoleNameExit(Sender: TObject);
    procedure cbIsActiveClick(Sender: TObject);
    procedure btnAssignAllClick(Sender: TObject);
    procedure btnAssignClick(Sender: TObject);
    procedure btnRemoveAllClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure listAvailablePrivilegesClick(Sender: TObject);
    procedure listAssignedPrivilegesClick(Sender: TObject);
    procedure listAssignedPrivilegesDblClick(Sender: TObject);
    procedure listAvailablePrivilegesDblClick(Sender: TObject);
  private
    FModels: TModels;
    FRolesInsertUpdate: TRolesInsertUpdateModel;
    procedure Refresh;
    procedure RefreshList;
    procedure EnableModifyButtons;
  public
    class procedure Execute(AModel: TModels;
      const AMode: TRolesInsertUpdateModes; const ARole: TRoleDetails);
  end;

var
  RolesInsertUpdate: TRolesInsertUpdate;

implementation

{$R *.dfm}

{ TRolesInsertUpdate }

procedure TRolesInsertUpdate.btnAssignAllClick(Sender: TObject);
begin
  FRolesInsertUpdate.AssignAllPrivileges;
  RefreshList;
end;

procedure TRolesInsertUpdate.btnAssignClick(Sender: TObject);
begin
  if listAvailablePrivileges.ItemIndex < 0 then Exit;

  FRolesInsertUpdate.AssignPrivilege(listAvailablePrivileges.ItemIndex);
  RefreshList;
end;

procedure TRolesInsertUpdate.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TRolesInsertUpdate.btnOkClick(Sender: TObject);
begin
  if FRolesInsertUpdate.Save then
    Close;
end;

procedure TRolesInsertUpdate.btnRemoveAllClick(Sender: TObject);
begin
  FRolesInsertUpdate.RemoveAllPrivileges;
  RefreshList;
end;

procedure TRolesInsertUpdate.btnRemoveClick(Sender: TObject);
begin
  if listAssignedPrivileges.ItemIndex < 0 then Exit;

  FRolesInsertUpdate.RemovePrivilege(listAssignedPrivileges.ItemIndex);
  RefreshList;
end;

procedure TRolesInsertUpdate.cbIsActiveClick(Sender: TObject);
begin
  FRolesInsertUpdate.IsActive := cbIsActive.Checked;
end;

procedure TRolesInsertUpdate.edtRoleNameExit(Sender: TObject);
begin
  FRolesInsertUpdate.Rolename := Trim(edtRoleName.Text);
end;

procedure TRolesInsertUpdate.EnableModifyButtons;
begin
  btnAssignAll.Enabled := FRolesInsertUpdate.IsBtnAssignAllEnabled;
  btnAssign.Enabled :=
    FRolesInsertUpdate.IsBtnAssignEnabled(listAvailablePrivileges.ItemIndex);
  btnRemoveAll.Enabled := FRolesInsertUpdate.IsBtnRemoveAllEnabled;
  btnRemove.Enabled :=
    FRolesInsertUpdate.IsBtnRemoveEnabled(listAssignedPrivileges.ItemIndex);
end;

class procedure TRolesInsertUpdate.Execute(AModel: TModels;
  const AMode: TRolesInsertUpdateModes; const ARole: TRoleDetails);
var
  i: Integer;
begin
  if RolesInsertUpdate = nil then
    RolesInsertUpdate := TRolesInsertUpdate.Create(Application);

  RolesInsertUpdate.FModels := AModel;
  RolesInsertUpdate.FRolesInsertUpdate := AModel.RolesInsertUpdate;
  RolesInsertUpdate.FRolesInsertUpdate.Initialize(AMode, ARole);
  RolesInsertUpdate.Show;
end;

procedure TRolesInsertUpdate.FormActivate(Sender: TObject);
begin
  Refresh;
end;

procedure TRolesInsertUpdate.listAssignedPrivilegesClick(Sender: TObject);
begin
  EnableModifyButtons;
end;

procedure TRolesInsertUpdate.listAssignedPrivilegesDblClick(Sender: TObject);
begin
//
end;

procedure TRolesInsertUpdate.listAvailablePrivilegesClick(Sender: TObject);
begin
  EnableModifyButtons;
end;

procedure TRolesInsertUpdate.listAvailablePrivilegesDblClick(Sender: TObject);
begin
 //
end;

procedure TRolesInsertUpdate.Refresh;
begin
  edtRoleName.Text := FRolesInsertUpdate.RoleDetails.RoleName;
  cbIsActive.Checked := FRolesInsertUpdate.RoleDetails.IsActive;

  FRolesInsertUpdate.RefreshLists;
  RefreshList;
end;

procedure TRolesInsertUpdate.RefreshList;
var
  i: Integer;
begin
  listAssignedPrivileges.Clear;
  listAvailablePrivileges.Clear;
  for i := 0 to Length(FRolesInsertUpdate.RoleDetails.Privileges) - 1 do
    listAssignedPrivileges.AddItem(FRolesInsertUpdate.RoleDetails.Privileges[i].Name,nil);
  for i := 0 to Length(FRolesInsertUpdate.AvailablePrivileges) - 1 do
    listAvailablePrivileges.AddItem(FRolesInsertUpdate.AvailablePrivileges[i].Name,nil);
  EnableModifyButtons;
end;

end.
