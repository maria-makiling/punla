unit RolesViewForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.StdCtrls
  , Vcl.ExtCtrls
  , Models
  , RolesViewModel
  ;

type
  TRolesView = class(TForm)
    pnlMain: TPanel;
    pnlDetails: TPanel;
    pnlProperties: TPanel;
    pnlRoleName: TPanel;
    pnlIsActive: TPanel;
    pnlPrivileges: TPanel;
    pnlAssignedPrivileges: TPanel;
    pnlAvailablePrivileges: TPanel;
    pnlControl: TPanel;
    pnlSettings: TPanel;
    pnlControlLeft: TPanel;
    pnlControlRight: TPanel;
    pnlRoles: TPanel;
    lblRoleName: TLabel;
    lblRoleNameField: TLabel;
    lblIsActive: TLabel;
    lblIsActiveField: TLabel;
    lblAssignedPrivileges: TLabel;
    lblAvailablePrivileges: TLabel;
    lblRoles: TLabel;
    listAssignedPrivileges: TListBox;
    listAvailablePrivileges: TListBox;
    listRoles: TListBox;
    cbActiveOnly: TCheckBox;
    btnNew: TButton;
    btnDuplicate: TButton;
    btnDelete: TButton;
    btnOk: TButton;
    btnEdit: TButton;
    procedure btnOkClick(Sender: TObject);
    procedure cbActiveOnlyClick(Sender: TObject);
    procedure listRolesClick(Sender: TObject);
    procedure listRolesDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure listRolesEnter(Sender: TObject);
    procedure listRolesDblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnDuplicateClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    FModels: TModels;
    FRolesView: TRolesViewModel;
    procedure Delete;
    procedure Duplicate;
    procedure Edit;
    procedure PopulateRoleList;
    procedure PopulateAssignedPrivileges;
    procedure PopulateAvailablePrivileges;
    procedure Refresh;
    procedure SetActiveSetting;
    procedure EnableControlButtons;
    procedure SetSelectedRole;
  public
    class procedure Execute(AModel: TModels; const ARoleID: Integer = -1);
  end;

var
  RolesView: TRolesView;

implementation

uses
    Utils
  , ProjectTypes
  , RolesInsertUpdateForm
  ;

{$R *.dfm}

{ TRolesView }

procedure TRolesView.btnDeleteClick(Sender: TObject);
begin
  Delete;
end;

procedure TRolesView.btnDuplicateClick(Sender: TObject);
begin
  Duplicate;
end;

procedure TRolesView.btnEditClick(Sender: TObject);
begin
  Edit;
end;

procedure TRolesView.btnNewClick(Sender: TObject);
var
  BlankRole: TRoleDetails;
begin
  if not FRolesView.HasInsertPrivileges then Exit;
  TRolesInsertUpdate.Execute(FModels, rmNew, BlankRole);
end;

procedure TRolesView.btnOkClick(Sender: TObject);
begin
  Close;
end;

procedure TRolesView.cbActiveOnlyClick(Sender: TObject);
begin
  SetActiveSetting;
end;

procedure TRolesView.Delete;
begin
  if listRoles.ItemIndex < 0 then Exit;
  if Utils.MsgDelete(listRoles.Items[listRoles.ItemIndex]) = mrYes then
    if not FRolesView.Delete(FRolesView.Roles[listRoles.ItemIndex].ID) then
      Utils.MsgWarning('An error occured trying to delete ' +
        listRoles.Items[listRoles.ItemIndex]);
  Refresh;
end;

procedure TRolesView.Duplicate;
begin
  if not FRolesView.HasInsertPrivileges then Exit;
  if listRoles.ItemIndex < 0 then Exit;

  TRolesInsertUpdate.Execute(FModels, rmDuplicate,
    FRolesView.GetRoleDetails(listRoles.ItemIndex));
end;

procedure TRolesView.Edit;
begin
  if not FRolesView.HasUpdatePrivileges then Exit;
  if listRoles.ItemIndex < 0 then Exit;

  TRolesInsertUpdate.Execute(FModels, rmEdit,
    FRolesView.GetRoleDetails(listRoles.ItemIndex));
end;

procedure TRolesView.EnableControlButtons;
begin
  btnNew.Enabled := FRolesView.HasInsertPrivileges;
  btnDuplicate.Enabled := FRolesView.HasInsertPrivileges and
    (listRoles.ItemIndex >= 0);
  btnEdit.Enabled := FRolesView.HasUpdatePrivileges and
    (listRoles.ItemIndex >= 0);
  btnDelete.Enabled := FRolesView.HasDeletePrivileges and
    (listRoles.ItemIndex >= 0);
end;

class procedure TRolesView.Execute(AModel: TModels; const ARoleID: Integer);
begin
  if RolesView = nil then
    RolesView := TRolesView.Create(Application);

  RolesView.FModels := AModel;
  RolesView.FRolesView := AModel.RolesView;
  RolesView.FRolesView.SelectedRoleID := ARoleID;
  RolesView.Show;
end;

procedure TRolesView.FormActivate(Sender: TObject);
begin
  Refresh;
end;

procedure TRolesView.listRolesClick(Sender: TObject);
begin
  SetSelectedRole;
end;

procedure TRolesView.listRolesDblClick(Sender: TObject);
begin
  Edit;
end;

procedure TRolesView.listRolesDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  Utils.DrawListboxItem(Control, FRolesView.Roles[Index].IsActive, Index, Rect);
end;

procedure TRolesView.listRolesEnter(Sender: TObject);
begin
  if listRoles.Items.Count <= 0 then
    Exit;

  if listRoles.ItemIndex < 0 then
    listRoles.ItemIndex := 0;
  SetSelectedRole;
end;

procedure TRolesView.PopulateAssignedPrivileges;
var
  i: Integer;
begin
  listAssignedPrivileges.Clear;
  for i := 0 to Length(FRolesView.AssignedPrivileges) - 1 do
    listAssignedPrivileges.AddItem(FRolesView.AssignedPrivileges[i].Name, nil);
  lblAssignedPrivileges.Caption := FRolesView.AssignedPrivilegesLabel;
end;

procedure TRolesView.PopulateAvailablePrivileges;
var
  i: Integer;
begin
  listAvailablePrivileges.Clear;
  for i := 0 to Length(FRolesView.AvailablePrivileges) - 1 do
    listAvailablePrivileges.AddItem(FRolesView.AvailablePrivileges[i].Name, nil);
  lblAvailablePrivileges.Caption := FRolesView.AvailablePrivilegesLabel;
end;

procedure TRolesView.PopulateRoleList;
var
  i: Integer;
begin
  listRoles.Clear;
  for i := 0 to Length(FRolesView.Roles) - 1 do
    listRoles.AddItem(FRolesView.Roles[i].Name, nil);
  lblRoles.Caption := FRolesView.RolesLabel;
end;

procedure TRolesView.Refresh;
var
  i: Integer;
begin
  FRolesView.Initialize;
  PopulateRoleList;
  if listRoles.Items.Count <= 0 then Exit;
  if listRoles.CanFocus then listRoles.SetFocus;
  listRoles.ItemIndex := FRolesView.GetSelectedRoleIndex;
  PopulateAssignedPrivileges;
  PopulateAvailablePrivileges;
  EnableControlButtons;
end;

procedure TRolesView.SetActiveSetting;
begin
  FRolesView.ShowActiveOnly := cbActiveOnly.Checked;
  Refresh;
end;

procedure TRolesView.SetSelectedRole;
begin
  if Length(FRolesView.Roles) <= 0 then Exit;
  if listRoles.ItemIndex < 0 then Exit;
  if listRoles.ItemIndex > (Length(FRolesView.Roles) - 1) then Exit;

  FRolesView.SelectedRoleID := FRolesView.Roles[listRoles.ItemIndex].ID;
  lblRoleNameField.Caption := FRolesView.Roles[listRoles.ItemIndex].Name;
  lblIsActiveField.Caption := Utils.IsActiveToStr(
    FRolesView.Roles[listRoles.ItemIndex].IsActive);

  PopulateAssignedPrivileges;
  PopulateAvailablePrivileges;
  EnableControlButtons;
end;

end.
