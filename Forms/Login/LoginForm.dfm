object Login: TLogin
  Left = 0
  Top = 0
  Caption = 'Login'
  ClientHeight = 261
  ClientWidth = 284
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 284
    Height = 261
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    TabOrder = 0
    object pnlLogo: TPanel
      Left = 12
      Top = 12
      Width = 260
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
    end
    object pnlDetails: TPanel
      Left = 12
      Top = 53
      Width = 260
      Height = 155
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 6
      TabOrder = 0
      object pnlServer: TPanel
        Left = 6
        Top = 6
        Width = 248
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 6
        TabOrder = 2
        DesignSize = (
          248
          36)
        object lblServer: TLabel
          Left = 6
          Top = 6
          Width = 70
          Height = 24
          Align = alLeft
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Server : '
          Layout = tlCenter
          ExplicitHeight = 28
        end
        object lblServerField: TLabel
          Left = 76
          Top = 11
          Width = 166
          Height = 13
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          Color = clWindow
          ParentColor = False
          Transparent = False
          Layout = tlCenter
        end
      end
      object pnlPassword: TPanel
        Left = 6
        Top = 114
        Width = 248
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 6
        TabOrder = 1
        DesignSize = (
          248
          36)
        object lblPassword: TLabel
          Left = 6
          Top = 6
          Width = 70
          Height = 24
          Align = alLeft
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Password : '
          Layout = tlCenter
          ExplicitHeight = 28
        end
        object edtPassword: TEdit
          Left = 76
          Top = 10
          Width = 166
          Height = 17
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          PasswordChar = '*'
          TabOrder = 0
          TextHint = 'Enter your password'
          OnMouseDown = edtPasswordMouseDown
          OnMouseUp = edtPasswordMouseUp
        end
      end
      object pnlUsername: TPanel
        Left = 6
        Top = 78
        Width = 248
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 6
        TabOrder = 0
        DesignSize = (
          248
          36)
        object lblUsername: TLabel
          Left = 6
          Top = 6
          Width = 70
          Height = 24
          Align = alLeft
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Username : '
          Layout = tlCenter
          ExplicitHeight = 28
        end
        object edtUsername: TEdit
          Left = 76
          Top = 10
          Width = 166
          Height = 17
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          TabOrder = 0
          TextHint = 'Enter your username'
        end
      end
      object pnlDatabase: TPanel
        Left = 6
        Top = 42
        Width = 248
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 6
        TabOrder = 3
        DesignSize = (
          248
          36)
        object lblDatabase: TLabel
          Left = 6
          Top = 6
          Width = 70
          Height = 24
          Align = alLeft
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Database : '
          Layout = tlCenter
          ExplicitHeight = 28
        end
        object lblDatabaseField: TLabel
          Left = 76
          Top = 11
          Width = 166
          Height = 13
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          Color = clWindow
          ParentColor = False
          Transparent = False
          Layout = tlCenter
        end
      end
    end
    object pnlControls: TPanel
      Left = 12
      Top = 208
      Width = 260
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 6
      TabOrder = 1
      object btnLogin: TButton
        Left = 6
        Top = 6
        Width = 248
        Height = 29
        Align = alClient
        Caption = 'Login'
        TabOrder = 0
        OnClick = btnLoginClick
      end
    end
  end
end
