unit LoginForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.ExtCtrls
  , Vcl.StdCtrls
  , Models
  , LoginModel
  ;

type
  TLogin = class(TForm)
    pnlMain: TPanel;
    pnlLogo: TPanel;
    pnlDetails: TPanel;
    pnlControls: TPanel;
    btnLogin: TButton;
    pnlServer: TPanel;
    pnlPassword: TPanel;
    pnlUsername: TPanel;
    pnlDatabase: TPanel;
    lblServer: TLabel;
    lblServerField: TLabel;
    lblDatabase: TLabel;
    lblDatabaseField: TLabel;
    lblPassword: TLabel;
    lblUsername: TLabel;
    edtUsername: TEdit;
    edtPassword: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
    procedure edtPasswordMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edtPasswordMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FLoginModel: TLoginModel;
  public
    class function Execute(AModel: TModels): boolean;
  end;

var
  Login: TLogin;

implementation

uses
    Constants
  , Utils
  ;

{$R *.dfm}

procedure TLogin.btnLoginClick(Sender: TObject);
begin
  if not FLoginModel.doTryLogin(edtUsername.Text, edtPassword.Text) then
    Utils.MsgWarning(MSG_INCORRECT_USERNAME_PASSWORD)
  else
    Close;
end;

procedure TLogin.edtPasswordMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then begin
    edtPassword.PasswordChar := #0;
  end;
end;

procedure TLogin.edtPasswordMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then begin
    edtPassword.PasswordChar := Char('*');
  end;
end;

class function TLogin.Execute(AModel: TModels): boolean;
begin
  if Login = nil then
    Login := TLogin.Create(Application);

  Login.FLoginModel := AModel.Login;
  try
    Login.ShowModal;
    Result := AModel.Global.Status.UserID > 0;
  finally
    FreeAndNil(Login);
  end;
end;

procedure TLogin.FormCreate(Sender: TObject);
begin
  lblServerField.Caption := DEFAULT_HOSTNAME;
  lblDatabaseField.Caption := DEFAULT_DATABASE;
  edtUsername.Text := DEFAULT_SUPERUSER;
  edtPassword.Text := DEFAULT_SUPERUSER;
end;

procedure TLogin.FormShow(Sender: TObject);
begin
  if not FLoginModel.doTryConnect then
    Utils.MsgWarning(MSG_CANT_CONNECT_DB);
end;

end.
