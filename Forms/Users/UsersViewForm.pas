unit UsersViewForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.StdCtrls
  , Vcl.ExtCtrls
  , Models
  , UsersViewModel
  ;

type
  TUsersView = class(TForm)
    pnlAssignedRoles: TPanel;
    pnlAvailableRoles: TPanel;
    pnlControl: TPanel;
    pnlDetails: TPanel;
    pnlIsActive: TPanel;
    pnlMain: TPanel;
    pnlProperties: TPanel;
    pnlRoles: TPanel;
    pnlSettings: TPanel;
    pnlUsername: TPanel;
    pnlUsers: TPanel;
    pnlSurname: TPanel;
    pnlGivenName: TPanel;
    pnlControlLeft: TPanel;
    pnlControlRight: TPanel;
    lblAssignedRoles: TLabel;
    lblAvailableRoles: TLabel;
    lblIsActive: TLabel;
    lblIsActiveField: TLabel;
    lblUsername: TLabel;
    lblUsernameField: TLabel;
    lblUsers: TLabel;
    lblSurname: TLabel;
    lblSurnameField: TLabel;
    lblGivenName: TLabel;
    lblGivenNameField: TLabel;
    listAssignedRoles: TListBox;
    listAvailableRoles: TListBox;
    listUsers: TListBox;
    cbActiveOnly: TCheckBox;
    btnDuplicate: TButton;
    btnDelete: TButton;
    btnEdit: TButton;
    btnNew: TButton;
    btnOk: TButton;
    procedure btnOkClick(Sender: TObject);
    procedure cbActiveOnlyClick(Sender: TObject);
    procedure listUsersClick(Sender: TObject);
    procedure listUsersDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure listUsersEnter(Sender: TObject);
    procedure listUsersDblClick(Sender: TObject);
    procedure listAssignedRolesDblClick(Sender: TObject);
    procedure listAvailableRolesDblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnDuplicateClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    FModels: TModels;
    FUsersView: TUsersViewModel;
    procedure Delete;
    procedure Duplicate;
    procedure Edit;
    procedure PopulateUserList;
    procedure PopulateAssignedRoles;
    procedure PopulateAvailableRoles;
    procedure Refresh;
    procedure SetActiveSetting;
    procedure EnableControlButtons;
    procedure SetSelectedUser;
  public
    class procedure Execute(AModel: TModels; const AUserID: Integer = -1);
  end;

var
  UsersView: TUsersView;

implementation

{$R *.dfm}

uses
    Utils
  , ProjectTypes
  , UsersInsertUpdateForm
  , RolesViewForm
  ;

{ TfrmUsersView }

procedure TUsersView.btnDeleteClick(Sender: TObject);
begin
  Delete;
end;

procedure TUsersView.btnDuplicateClick(Sender: TObject);
begin
  Duplicate;
end;

procedure TUsersView.btnEditClick(Sender: TObject);
begin
  Edit;
end;

procedure TUsersView.btnNewClick(Sender: TObject);
var
  BlankUser: TUserDetails;
begin
  if not FUsersView.HasInsertPrivileges then Exit;
  TUsersInsertUpdate.Execute(FModels, umNew, BlankUser);
end;

procedure TUsersView.btnOkClick(Sender: TObject);
begin
  Close;
end;

procedure TUsersView.cbActiveOnlyClick(Sender: TObject);
begin
  SetActiveSetting;
end;

procedure TUsersView.Delete;
begin
  if listUsers.ItemIndex < 0 then Exit;
  if Utils.MsgDelete(listUsers.Items[listUsers.ItemIndex]) = mrYes then
    if not FUsersView.Delete(FUsersView.Users[listUsers.ItemIndex].PartyID) then
      Utils.MsgWarning('An error occured trying to delete ' +
        listUsers.Items[listUsers.ItemIndex]);
  Refresh;
end;

procedure TUsersView.Duplicate;
begin
  if not FUsersView.HasInsertPrivileges then Exit;
  if listUsers.ItemIndex < 0 then Exit;
  TUsersInsertUpdate.Execute(FModels, umDuplicate,
    FUsersView.GetUserDetails(listUsers.ItemIndex));
end;

procedure TUsersView.Edit;
begin
  if not FUsersView.HasUpdatePrivileges then Exit;
  if listUsers.ItemIndex < 0 then Exit;

  TUsersInsertUpdate.Execute(FModels, umEdit,
    FUsersView.GetUserDetails(listUsers.ItemIndex));
end;

procedure TUsersView.EnableControlButtons;
begin
  btnNew.Enabled := FUsersView.HasInsertPrivileges;
  btnDuplicate.Enabled := FUsersView.HasInsertPrivileges and
    (listUsers.ItemIndex >= 0);
  btnEdit.Enabled := FUsersView.HasUpdatePrivileges and
    (listUsers.ItemIndex >= 0);
  btnDelete.Enabled := FUsersView.HasDeletePrivileges and
    (listUsers.ItemIndex >= 0);
end;

class procedure TUsersView.Execute(AModel: TModels;
  const AUserID: Integer = -1);
begin
  if UsersView = nil then
    UsersView := TUsersView.Create(Application);

  UsersView.FModels := AModel;
  UsersView.FUsersView := AModel.UsersView;
  UsersView.FUsersView.SelectedUserID := AUserID;
  UsersView.Show;
end;

procedure TUsersView.FormActivate(Sender: TObject);
begin
  Refresh;
end;

procedure TUsersView.listAssignedRolesDblClick(Sender: TObject);
begin
  if not FModels.Global.UserPrivileges.HasRolesView then Exit;
  if listAssignedRoles.ItemIndex < 0 then Exit;

  TRolesView.Execute(
    FModels,
    FUsersView.AssignedRoles[listAssignedRoles.ItemIndex].ID);
end;

procedure TUsersView.listAvailableRolesDblClick(Sender: TObject);
begin
  if not FModels.Global.UserPrivileges.HasRolesView then Exit;
  if listAvailableRoles.ItemIndex < 0 then Exit;

  TRolesView.Execute(
    FModels,
    FUsersView.AvailableRoles[listAvailableRoles.ItemIndex].ID);
end;

procedure TUsersView.listUsersClick(Sender: TObject);
begin
  SetSelectedUser;
end;

procedure TUsersView.listUsersDblClick(Sender: TObject);
begin
  Edit;
end;

procedure TUsersView.listUsersDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  Utils.DrawListboxItem(Control, FUsersView.Users[Index].IsActive, Index, Rect);
end;

procedure TUsersView.listUsersEnter(Sender: TObject);
begin
  if listUsers.Items.Count <= 0 then
    Exit;

  if listUsers.ItemIndex < 0 then
    listUsers.ItemIndex := 0;
  SetSelectedUser;
end;

procedure TUsersView.PopulateUserList;
var
  i: Integer;
begin
  listUsers.Clear;
  for i := 0 to Length(FUsersView.Users) - 1 do
    listUsers.AddItem(FUsersView.Users[i].Username, nil);
  lblUsers.Caption := FUsersView.UsersLabel;
end;

procedure TUsersView.PopulateAssignedRoles;
var
  i: Integer;
begin
  listAssignedRoles.Clear;
  for i := 0 to Length(FUsersView.AssignedRoles) - 1 do
    listAssignedRoles.AddItem(FUsersView.AssignedRoles[i].Name, nil);
  lblAssignedRoles.Caption := FUsersView.AssignedRolesLabel;
end;

procedure TUsersView.PopulateAvailableRoles;
var
  i: Integer;
begin
  listAvailableRoles.Clear;
  for i := 0 to Length(FUsersView.AvailableRoles) - 1 do
    listAvailableRoles.AddItem(FUsersView.AvailableRoles[i].Name, nil);
  lblAvailableRoles.Caption := FUsersView.AvailableRolesLabel;
end;

procedure TUsersView.Refresh;
var
  i: Integer;
begin
  FUsersView.Initialize;
  PopulateUserList;
  if listUsers.Items.Count <= 0 then Exit;
  if listUsers.CanFocus then listUsers.SetFocus;
  PopulateAssignedRoles;
  PopulateAvailableRoles;
  EnableControlButtons;
end;

procedure TUsersView.SetActiveSetting;
begin
  FUsersView.ShowActiveOnly := cbActiveOnly.Checked;
  Refresh;
end;

procedure TUsersView.SetSelectedUser;
begin
  if Length(FUsersView.Users) <= 0 then Exit;
  if listUsers.ItemIndex < 0 then Exit;
  if listUsers.ItemIndex > (Length(FUsersView.Users) - 1) then Exit;

  FUsersView.SelectedUserID := FUsersView.Users[listUsers.ItemIndex].PartyID;
  lblGivenNameField.Caption := FUsersView.Persons[listUsers.ItemIndex].GivenName;
  lblSurnameField.Caption := FUsersView.Persons[listUsers.ItemIndex].Surname;
  lblUsernameField.Caption := FUsersView.Users[listUsers.ItemIndex].Username;
  lblIsActiveField.Caption := Utils.IsActiveToStr(
    FUsersView.Users[listUsers.ItemIndex].IsActive);

  PopulateAssignedRoles;
  PopulateAvailableRoles;
  EnableControlButtons;
end;

end.
