unit UsersInsertUpdateForm;

interface

uses
    Winapi.Windows
  , Winapi.Messages
  , System.SysUtils
  , System.Variants
  , System.Classes
  , Vcl.Graphics
  , Vcl.Controls
  , Vcl.Forms
  , Vcl.Dialogs
  , Vcl.StdCtrls
  , Vcl.ExtCtrls
  , Models
  , UsersInsertUpdateModel
  , UsersViewModel
  , ProjectTypes
  ;

type
  TUsersInsertUpdate = class(TForm)
    pnlMain: TPanel;
    pnlDetails: TPanel;
    pnlProperties: TPanel;
    pnlUsername: TPanel;
    pnlPassword: TPanel;
    pnlPasswordConfirm: TPanel;
    pnlIsActive: TPanel;
    pnlSurname: TPanel;
    pnlGivenName: TPanel;
    pnlRoles: TPanel;
    pnlAssignedRoles: TPanel;
    pnlAvailableRoles: TPanel;
    pnlModify: TPanel;
    pnlModifyButtons: TPanel;
    pnlControl: TPanel;
    lblUsername: TLabel;
    lblPassword: TLabel;
    lblPasswordConfirm: TLabel;
    lblIsActive: TLabel;
    lblSurname: TLabel;
    lblGivenName: TLabel;
    lblAssignedRoles: TLabel;
    lblAvailableRoles: TLabel;
    listAssignedRoles: TListBox;
    listAvailableRoles: TListBox;
    cbIsActive: TCheckBox;
    btnRemoveAll: TButton;
    btnAssignAll: TButton;
    btnAssign: TButton;
    btnRemove: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    edtUsername: TEdit;
    edtPassword: TEdit;
    edtPasswordConfirm: TEdit;
    edtSurname: TEdit;
    edtGivenName: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtGivenNameExit(Sender: TObject);
    procedure edtSurnameExit(Sender: TObject);
    procedure edtUsernameExit(Sender: TObject);
    procedure edtPasswordExit(Sender: TObject);
    procedure edtPasswordConfirmExit(Sender: TObject);
    procedure cbIsActiveClick(Sender: TObject);
    procedure btnAssignAllClick(Sender: TObject);
    procedure btnAssignClick(Sender: TObject);
    procedure btnRemoveAllClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure listAvailableRolesClick(Sender: TObject);
    procedure listAssignedRolesClick(Sender: TObject);
    procedure listAssignedRolesDblClick(Sender: TObject);
    procedure listAvailableRolesDblClick(Sender: TObject);
  private
    FModels: TModels;
    FUsersInsertUpdate: TUsersInsertUpdateModel;
    procedure Refresh;
    procedure RefreshList;
    procedure EnableModifyButtons;
  public
    class procedure Execute(AModel: TModels;
      const AMode: TUsersInsertUpdateModes;
      const AUser: TUserDetails);
  end;

var
  UsersInsertUpdate: TUsersInsertUpdate;

implementation

uses
    RolesViewForm
  ;


{$R *.dfm}

{ TUsersInsertUpdate }

procedure TUsersInsertUpdate.btnAssignAllClick(Sender: TObject);
begin
  FUsersInsertUpdate.AssignAllRoles;
  RefreshList;
end;

procedure TUsersInsertUpdate.btnAssignClick(Sender: TObject);
begin
  if listAvailableRoles.ItemIndex < 0 then Exit;

  FUsersInsertUpdate.AssignRole(listAvailableRoles.ItemIndex);
  RefreshList;
end;

procedure TUsersInsertUpdate.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TUsersInsertUpdate.btnOkClick(Sender: TObject);
begin
  if FUsersInsertUpdate.Save then
    Close;
end;

procedure TUsersInsertUpdate.btnRemoveAllClick(Sender: TObject);
begin
  FUsersInsertUpdate.RemoveAllRoles;
  RefreshList;
end;

procedure TUsersInsertUpdate.btnRemoveClick(Sender: TObject);
begin
  if listAssignedRoles.ItemIndex < 0 then Exit;

  FUsersInsertUpdate.RemoveRole(listAssignedRoles.ItemIndex);
  RefreshList;
end;

procedure TUsersInsertUpdate.cbIsActiveClick(Sender: TObject);
begin
  FUsersInsertUpdate.IsActive := cbIsActive.Checked;
end;

procedure TUsersInsertUpdate.edtGivenNameExit(Sender: TObject);
begin
  FUsersInsertUpdate.GivenName := Trim(edtGivenName.Text);
end;

procedure TUsersInsertUpdate.edtPasswordConfirmExit(Sender: TObject);
begin
  FUsersInsertUpdate.ConfirmNewPassword := Trim(edtPasswordConfirm.Text);
end;

procedure TUsersInsertUpdate.edtPasswordExit(Sender: TObject);
begin
  FUsersInsertUpdate.NewPassword := Trim(edtPassword.Text);
end;

procedure TUsersInsertUpdate.edtSurnameExit(Sender: TObject);
begin
  FUsersInsertUpdate.Surname := Trim(edtSurname.Text);
end;

procedure TUsersInsertUpdate.edtUsernameExit(Sender: TObject);
begin
  FUsersInsertUpdate.Username := Trim(edtUsername.Text);
end;

procedure TUsersInsertUpdate.EnableModifyButtons;
begin
  btnAssignAll.Enabled := FUsersInsertUpdate.IsBtnAssignAllEnabled;
  btnAssign.Enabled :=
    FUsersInsertUpdate.IsBtnAssignEnabled(listAvailableRoles.ItemIndex);
  btnRemoveAll.Enabled := FUsersInsertUpdate.IsBtnRemoveAllEnabled;
  btnRemove.Enabled :=
    FUsersInsertUpdate.IsBtnRemoveEnabled(listAssignedRoles.ItemIndex);
end;

class procedure TUsersInsertUpdate.Execute(AModel: TModels;
  const AMode: TUsersInsertUpdateModes; const AUser: TUserDetails);
var
  i: Integer;
begin
  if UsersInsertUpdate = nil then
    UsersInsertUpdate := TUsersInsertUpdate.Create(Application);

  UsersInsertUpdate.FModels := AModel;
  UsersInsertUpdate.FUsersInsertUpdate := AModel.UsersInsertUpdate;
  UsersInsertUpdate.FUsersInsertUpdate.Initialize(AMode, AUser);
  UsersInsertUpdate.Show;
end;

procedure TUsersInsertUpdate.FormActivate(Sender: TObject);
begin
  Refresh;
end;

procedure TUsersInsertUpdate.listAssignedRolesClick(Sender: TObject);
begin
  EnableModifyButtons;
end;

procedure TUsersInsertUpdate.listAssignedRolesDblClick(Sender: TObject);
begin
  if not FModels.Global.UserPrivileges.HasRolesView then Exit;
  if listAssignedRoles.ItemIndex < 0 then Exit;

  TRolesView.Execute(
    FModels,
    FUsersInsertUpdate.UserDetails.Roles[listAssignedRoles.ItemIndex].ID);
end;

procedure TUsersInsertUpdate.listAvailableRolesClick(Sender: TObject);
begin
  EnableModifyButtons;
end;

procedure TUsersInsertUpdate.listAvailableRolesDblClick(Sender: TObject);
begin
  if not FModels.Global.UserPrivileges.HasRolesView then Exit;
  if listAvailableRoles.ItemIndex < 0 then Exit;

  TRolesView.Execute(
    FModels,
    FUsersInsertUpdate.AvailableRoles[listAvailableRoles.ItemIndex].ID);
end;

procedure TUsersInsertUpdate.Refresh;
begin
  edtGivenName.Text := FUsersInsertUpdate.UserDetails.GivenName;
  edtSurname.Text := FUsersInsertUpdate.UserDetails.Surname;
  edtUsername.Text := FUsersInsertUpdate.UserDetails.Username;
  cbIsActive.Checked := FUsersInsertUpdate.UserDetails.IsActive;

  FUsersInsertUpdate.RefreshLists;
  RefreshList;
end;

procedure TUsersInsertUpdate.RefreshList;
var
  i: Integer;
begin
  listAssignedRoles.Clear;
  listAvailableRoles.Clear;
  for i := 0 to Length(FUsersInsertUpdate.UserDetails.Roles) - 1 do
    listAssignedRoles.AddItem(FUsersInsertUpdate.UserDetails.Roles[i].Name,nil);
  for i := 0 to Length(FUsersInsertUpdate.AvailableRoles) - 1 do
    listAvailableRoles.AddItem(FUsersInsertUpdate.AvailableRoles[i].Name,nil);
  EnableModifyButtons;
end;

end.
