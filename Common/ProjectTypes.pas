unit ProjectTypes;

interface

{$M+}

type
  TStringArray = Array of String;

  TUsersInsertUpdateModes = (umNew, umDuplicate, umEdit);
  TRolesInsertUpdateModes = (rmNew, rmDuplicate, rmEdit);

  TBaseType = class;
  TBaseTypes = array of TBaseType;
  TUser = class;
  TUsers = array of TUser;
  TRole = class;
  TRoles = array of TRole;
  TPrivilege = class;
  TPrivileges = array of TPrivilege;
  TPerson = class;
  TPersons = array of TPerson;

  TBaseType = class
  public
    function IsDuplicate(const ABaseType: TBaseType): Boolean; virtual;
    function Find(const AHaystack: TBaseTypes): Boolean;
  end;

  TUser = class
  private
    FPartyID: Integer;
    FUsername: String;
    FPassword: String;
    FIsActive: Boolean;
  public
    constructor Create(const APartyID: Integer; const AUsername: String;
      const APassword: String; const AIsActive: Boolean);
  published
    property PartyID: Integer read FPartyID;
    property Username: String read FUsername;
    property Password: String read FPassword;
    property IsActive: Boolean read FIsActive;
  end;

  TRole = class
  private
    FID: Integer;
    FName: String;
    FIsActive: Boolean;
  public
    constructor Create(const AID: Integer; const AName: String;
      const AIsActive: Boolean);
    function IsDuplicate(const ARole: TRole): Boolean;
    function Find(const AHaystack: TRoles): Boolean;
  published
    property ID: Integer read FID;
    property Name: String read FName;
    property IsActive: Boolean read FIsActive;
  end;

  TPrivilege = class
  private
    FID: Integer;
    FName: String;
    FDescription: String;
  public
    constructor Create(const AID: Integer; const AName: String;
      const ADescription: String);
    function IsDuplicate(const APrivilege: TPrivilege): Boolean;
    function Find(const AHaystack: TPrivileges): Boolean;
  published
    property ID: Integer read FID;
    property Name: String read FName;
    property Description: String read FDescription;
  end;

  TPerson = class
  private
    FPartyID: Integer;
    FGivenName: String;
    FSurname: String;
  public
    constructor Create(const APartyID: Integer; const AGivenName: String;
      const ASurname: String);
  published
    property PartyID: Integer read FPartyID;
    property GivenName: String read FGivenName;
    property Surname: String read FSurname;
  end;

  TUserDetails = record
    PartyID: Integer;
    GivenName: String;
    Surname: String;
    Username: String;
    IsActive: Boolean;
    Roles: TRoles;
  End;

  TRoleDetails = record
    ID: Integer;
    RoleName: String;
    IsActive: Boolean;
    Privileges: TPrivileges;
  End;

implementation

{ TUser }

constructor TUser.Create(const APartyID: Integer; const AUsername,
  APassword: String; const AIsActive: Boolean);
begin
  FPartyID := APartyID;
  FUsername := AUsername;
  FPassword := APassword;
  FIsActive := AIsActive;
end;

{ TRole }

constructor TRole.Create(const AID: Integer; const AName: String;
  const AIsActive: Boolean);
begin
  FID := AID;
  FName := AName;
  FIsActive := AIsActive;
end;

function TRole.Find(const AHaystack: TRoles): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to Length(AHaystack) - 1 do
  begin
    if AHaystack[i].IsDuplicate(Self) then Exit;
  end;
  Result := False;
end;

function TRole.IsDuplicate(const ARole: TRole): Boolean;
begin
  Result := False;
  if Self.FID <> ARole.FID then Exit;
  if Self.FName <> ARole.FName then Exit;
  if Self.FIsActive <> ARole.FIsActive then Exit;
  Result := True;
end;

{ TPrivilege }

constructor TPrivilege.Create(const AID: Integer; const AName,
  ADescription: String);
begin
  FID := AID;
  FName := AName;
  FDescription := ADescription;
end;

function TPrivilege.Find(const AHaystack: TPrivileges): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to Length(AHaystack) - 1 do
  begin
    if AHaystack[i].IsDuplicate(Self) then Exit;
  end;
  Result := False;
end;

function TPrivilege.IsDuplicate(const APrivilege: TPrivilege): Boolean;
begin
  Result := False;
  if Self.FID <> APrivilege.FID then Exit;
  if Self.FName <> APrivilege.FName then Exit;
  if Self.FDescription <> APrivilege.FDescription then Exit;
  Result := True;
end;

{ TPerson }

constructor TPerson.Create(const APartyID: Integer; const AGivenName,
  ASurname: String);
begin
  FPartyID := APartyID;
  FGivenName := AGivenName;
  FSurname := ASurname;
end;

{ TBaseType }

function TBaseType.Find(const AHaystack: TBaseTypes): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to Length(AHaystack) - 1 do
  begin
    if AHaystack[i].IsDuplicate(Self) then Exit;
  end;
  Result := False;
end;

function TBaseType.IsDuplicate(const ABaseType: TBaseType): Boolean;
begin
  Result := False;
  if Self = ABaseType then Exit;
  Result := True;
end;

end.
