unit Global;

interface

uses
    ZConnection
  ;

{$M+}

type
  TGlobal = class;
  TStatus = class;
  TUsersPrivileges = class;
  TVersion = class;

  TGlobal = class
    private
      FStatus: TStatus;
      FUserPrivileges: TUsersPrivileges;
      FVersion: TVersion;
      FConnection: TZConnection;
    public
      constructor Create;
      destructor Destroy; override;
    published
      property Status: TStatus read FStatus write FStatus;
      property UserPrivileges: TUsersPrivileges read FUserPrivileges write FUserPrivileges;
      property Version: TVersion read FVersion write FVersion;
      property Connection: TZConnection read FConnection write FConnection;
  end;

  TStatus = class
    private
      FUserID: Integer;
      FHostName: String;
      FDatabase: String;
      FUsername: String;
    public
      constructor Create;
    published
      property UserID: Integer read FUserID write FUserID;
      property HostName: String read FHostName write FHostName;
      property Database: String read FDatabase write FDatabase;
      property Username: String read FUsername write FUsername;
  end;

  TUsersPrivileges = class
    private
      FHasSettingsView: Boolean;
      FHasSettingsUpdate: Boolean;
      FHasUsersView: Boolean;
      FHasUsersInsert: Boolean;
      FHasUsersUpdate: Boolean;
      FHasUsersDelete: Boolean;
      FHasRolesView: Boolean;
      FHasRolesInsert: Boolean;
      FHasRolesUpdate: Boolean;
      FHasRolesDelete: Boolean;
    public
      constructor Create;
    published
      Property HasSettingsView: Boolean read FHasSettingsView write FHasSettingsView;
      Property HasSettingsUpdate: Boolean read FHasSettingsUpdate write FHasSettingsUpdate;
      Property HasUsersView: Boolean read FHasUsersView write FHasUsersView;
      Property HasUsersInsert: Boolean read FHasUsersInsert write FHasUsersInsert;
      Property HasUsersUpdate: Boolean read FHasUsersUpdate write FHasUsersUpdate;
      Property HasUsersDelete: Boolean read FHasUsersDelete write FHasUsersDelete;
      Property HasRolesView: Boolean read FHasRolesView write FHasRolesView;
      Property HasRolesInsert: Boolean read FHasRolesInsert write FHasRolesInsert;
      Property HasRolesUpdate: Boolean read FHasRolesUpdate write FHasRolesUpdate;
      Property HasRolesDelete: Boolean read FHasRolesDelete write FHasRolesDelete;
  end;

  TVersion = class
    private
      FDBMajorVersion: Integer;
      FDBMinorVersion: Integer;
      FAppMajorVersion: Integer;
      FAppMinorVersion: Integer;
      FAppBuildVersion: Integer;
      FAppRevisionVersion: Integer;
    public
      constructor Create;
      function GetAppVersion: String;
      function GetDBVersion: String;
    published
      property AppVersion: String read GetAppVersion;
      property DBVersion: String read GetDBVersion;
      property DBMajorVersion: Integer read FDBMajorVersion write FDBMajorVersion;
      property DBMinorVersion: Integer read FDBMinorVersion write FDBMinorVersion;
      property AppMajorVersion: Integer read FAppMajorVersion write FAppMajorVersion;
      property AppMinorVersion: Integer read FAppMinorVersion write FAppMinorVersion;
      property AppBuildVersion: Integer read FAppBuildVersion write FAppBuildVersion;
      property AppRevisionVersion: Integer read FAppRevisionVersion write FAppRevisionVersion;
  end;

implementation

uses
    Constants
  , System.SysUtils
  ;

{ TGlobal }

constructor TGlobal.Create;
begin
  FStatus := TStatus.Create;
  FUserPrivileges := TUsersPrivileges.Create;
  FVersion := TVersion.Create;

  FConnection := TZConnection.Create(Nil);
  FConnection.HostName := DEFAULT_HOSTNAME;
  FConnection.Database := DEFAULT_DATABASE;
  FConnection.Password := DEFAULT_CONNECTION_PASSWORD;
  FConnection.Protocol := DEFAULT_CONNECTION_PROTOCOL;
  FConnection.User := PROJECT_NAME;
end;

destructor TGlobal.Destroy;
begin
  if Assigned(FConnection) then FreeAndNil(FConnection);
  if Assigned(FVersion) then FreeAndNil(FVersion);
  if Assigned(FUserPrivileges) then FreeAndNil(FUserPrivileges);
  if Assigned(FStatus) then FreeAndNil(FStatus);
  inherited;
end;

{ TStatus }

constructor TStatus.Create;
begin
  FUserID := -1;
  FHostName := DEFAULT_HOSTNAME;
  FDatabase := DEFAULT_DATABASE;
  FUsername := DEFAULT_SUPERUSER;
end;

{ TUsersPrivileges }

constructor TUsersPrivileges.Create;
begin
  FHasSettingsView := False;
  FHasSettingsUpdate := False;
  FHasUsersView := False;
  FHasUsersInsert := False;
  FHasUsersUpdate := False;
  FHasUsersDelete := False;
  FHasRolesView := False;
  FHasRolesInsert := False;
  FHasRolesUpdate := False;
  FHasRolesDelete := False;
end;

{ TVersion }

constructor TVersion.Create;
begin
  FDBMajorVersion := 0;
  FDBMinorVersion := 0;
  FAppMajorVersion := 0;
  FAppMinorVersion := 0;
  FAppBuildVersion := 0;
  FAppRevisionVersion := 0;
end;

function TVersion.GetAppVersion: String;
begin
  Result :=
    IntToStr(AppMajorVersion) + '.' +
    IntToStr(AppMinorVersion) + '.' +
    IntToStr(AppBuildVersion) + '.' +
    IntToStr(AppRevisionVersion);
end;

function TVersion.GetDBVersion: String;
begin
  Result :=
    IntToStr(DBMajorVersion) + '.' +
    IntToStr(DBMinorVersion);
end;

end.
