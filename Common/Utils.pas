unit Utils;

interface

uses
    Vcl.Dialogs
  , Vcl.Controls
  , Vcl.StdCtrls
  , System.UITypes
  , System.Types
  , ProjectTypes
  ;

//dialog messages
function MsgDelete(const AItem: String): Integer;
procedure MsgWarning(const AMsg: String);
//conversions
function IntToBool(const Value: Integer): Boolean;
function BoolToInt(Value: Boolean): integer;
//listbox
function IsActiveToColor(const Value: Boolean): TColor;
procedure DrawListboxItem(Control: TWinControl; IsActive: Boolean;
  Index: Integer; Rect: TRect);
//labels
function IsActiveToStr(const Value: Boolean): String;
function ListActiveNameNum(const ShowActiveOnly: Boolean; const Name: String;
  Num: Integer): String;
function ListNameNum(const Name: String; Num: Integer): String;
//compare arrays
function IsSameList(const ListA, ListB: TPrivileges): Boolean; Overload;
function IsSameList(const ListA, ListB: TRoles): Boolean; Overload;

implementation

uses
    Constants
  , System.SysUtils
  ;


function MsgDelete(const AItem: String): Integer;
begin
  Result := MessageDlg('Are you sure you want to permanently delete ' +
    AItem + '?', mtConfirmation,[mbNo, mbYes], 0,
    mbYes)
end;

procedure MsgWarning(const AMsg: String);
begin
  MessageDlg(AMsg, mtWarning,[mbOK], 0, mbOK)
end;

function IntToBool(const Value: Integer): Boolean;
begin
  if Value = 0 then
    Result := False
  else
    Result := True;
end;

function BoolToInt(Value: Boolean): integer;
begin
  if Value then
    Result := 1
  else
    Result := 0;
end;

function IsActiveToColor(const Value: Boolean): TColor;
begin
  if Value then
    Result := TColorRec.Black
  else
    Result := TColorRec.Gray;
end;

procedure DrawListboxItem(Control: TWinControl; IsActive: Boolean;
  Index: Integer; Rect: TRect);
var
  newRect: TRect;
begin
  with Control as TListBox do
  begin
    newRect := TRect.Create(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom + 1);
    Canvas.FillRect(newRect);
    Canvas.Font.Color := Utils.IsActiveToColor(IsActive);
    Canvas.TextOut(Rect.Left, Rect.Top, Items[Index]);
  end;
end;

function IsActiveToStr(const Value: Boolean): String;
begin
  if Value then
    Result := IS_ACTIVE_DISPLAY_TRUE
  else
    Result := IS_ACTIVE_DISPLAY_FALSE;
end;

function ListActiveNameNum(const ShowActiveOnly: Boolean; const Name: String;
  Num: Integer): String;
begin
  if ShowActiveOnly then
    Result := LABEL_ACTIVE_ONLY + ' ' + ListNameNum(Name, Num)
  else
    Result := LABEL_ALL + ' ' + ListNameNum(Name, Num);
end;

function ListNameNum(const Name: String; Num: Integer): String;
begin
  Result := Name + ' (' + IntToStr(Num) + ')';
end;

function IsSameList(const ListA, ListB: TPrivileges): Boolean; overload;
var
  i: Integer;
begin
  Result := False;
  if Length(ListA) <> Length(ListB) then
    Exit;
  for i := 0 to Length(ListA) - 1 do
    if not ListA[i].IsDuplicate(ListB[i]) then
      Exit;
  Result := True;
end;

function IsSameList(const ListA, ListB: TRoles): Boolean; overload;
var
  i: Integer;
begin
  Result := False;
  if Length(ListA) <> Length(ListB) then
    Exit;
  for i := 0 to Length(ListA) - 1 do
    if not ListA[i].IsDuplicate(ListB[i]) then
      Exit;
  Result := True;
end;

end.
