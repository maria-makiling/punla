unit Constants;

interface

const
  RUN_SELF_TEST = True;

  //general
  PROJECT_NAME = 'punla';
  DEFAULT_HOSTNAME = 'localhost';
  DEFAULT_DATABASE = PROJECT_NAME + 'db';
  DEFAULT_SUPERUSER = 'admin';
  DEFAULT_CONNECTION_PASSWORD = 'ASeedIsAForestInsideOut';
  DEFAULT_CONNECTION_PROTOCOL = 'mysql';

  //dialog messages
  MSG_CANT_CONNECT_DB = 'Cannot connect to database';
  MSG_INCORRECT_USERNAME_PASSWORD = 'Incorrect username or password';

  //generic DB
  DB_IS_ACTIVE = 'is_active';
  DB_CREATED_DATE = 'created_date';

  //form labels
  LABEL_USERS = 'Users';
  LABEL_ASSIGNED_ROLES = 'Assigned Roles';
  LABEL_AVAILABLE_ROLES = 'Available Roles';

  LABEL_ROLES = 'Roles';
  LABEL_ASSIGNED_PRIVILEGES = 'Assigned Privileges';
  LABEL_AVAILABLE_PRIVILEGES = 'Available Privileges';

  //listbox labels
  IS_ACTIVE_DISPLAY_TRUE = 'Active';
  IS_ACTIVE_DISPLAY_FALSE = 'Deactivated';
  LABEL_ACTIVE_ONLY = 'Active';
  LABEL_ALL = 'All';

  //privileges
  PRIVILEGE_SETTINGS_VIEW = 'settings_view';
  PRIVILEGE_SETTINGS_UPDATE = 'settings_update';
  PRIVILEGE_USERS_VIEW = 'users_view';
  PRIVILEGE_USERS_INSERT = 'users_insert';
  PRIVILEGE_USERS_UPDATE = 'users_update';
  PRIVILEGE_USERS_DELETE = 'users_delete';
  PRIVILEGE_ROLES_VIEW = 'roles_view';
  PRIVILEGE_ROLES_INSERT = 'roles_insert';
  PRIVILEGE_ROLES_UPDATE = 'roles_update';
  PRIVILEGE_ROLES_DELETE = 'roles_delete';

  //tables
  USERS_DB_TABLENAME = 'users';
  USERS_DB_PARTY_ID = 'party_id';
  USERS_DB_USERNAME = 'username';
  USERS_DB_PASSWORD = 'password';

  VERSIONS_DB_TABLENAME = 'versions';
  VERSIONS_DB_ID = 'id';
  VERSIONS_DB_DATABASE_MAJOR = 'database_major';
  VERSIONS_DB_DATABASE_MINOR = 'database_minor';
  VERSIONS_DB_APPLICATION_MAJOR = 'application_major';
  VERSIONS_DB_APPLICATION_MINOR = 'application_minor';
  VERSIONS_DB_APPLICATION_BUILD = 'application_build';
  VERSIONS_DB_APPLICATION_REVISION = 'application_revision';

  PRIVILEGES_DB_TABLENAME = 'privileges';
  PRIVILEGES_DB_ID = 'id';
  PRIVILEGES_DB_NAME = 'name';
  PRIVILEGES_DB_DESCRIPTION = 'description';

  PARTIES_DB_TABLENAME = 'parties';
  PARTIES_DB_ID = 'id';
  PARTIES_DB_CREATEDATE = 'created_date';

  ROLES_DB_TABLENAME = 'roles';
  ROLES_DB_ID = 'id';
  ROLES_DB_NAME = 'name';

  USERROLES_DB_TABLENAME = 'user_roles';
  USERROLES_DB_USER_ID = 'user_id';
  USERROLES_DB_ROLE_ID = 'role_id';

  ROLEPRIVILEGES_DB_TABLENAME = 'role_privileges';
  ROLEPRIVILEGES_DB_ROLE_ID = 'role_id';
  ROLEPRIVILEGES_DB_PRIVILEGE_ID = 'privilege_id';

  PERSONS_DB_TABLENAME = 'persons';
  PERSONS_DB_PARTY_ID = 'party_id';
  PERSONS_DB_GIVEN_NAME = 'given_name';
  PERSONS_DB_SURNAME = 'surname';

implementation

end.
