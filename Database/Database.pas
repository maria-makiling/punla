unit Database;

interface

uses
    System.SysUtils
  , ZConnection
  , UsersDB
  , VersionDB
  , PartiesDB
  , RolesDB
  , UserRolesDB
  , PrivilegesDB
  , RolePrivilegesDB
  , UsersUserRolesRolesRolePrivilegesPrivilegesDB
  , PersonsDB
  , RolesUserRolesDB
  , PrivilegesRolePrivilegesDB
  ;

{$M+}

type
  TDatabase = class
    DBConnection: TZConnection;
  private
  public
    Users: TUsersDB;
    Versions: TVersionsDB;
    Parties: TPartiesDB;
    Roles: TRolesDB;
    UserRoles: TUserRolesDB;
    Privileges: TPrivilegesDB;
    RolePrivileges: TRolePrivilegesDB;
    UsersUserRolesRolesRolePrivilegesPrivileges:
      TUsersUserRolesRolesRolePrivilegesPrivilegesDB;
    Persons: TPersonsDB;
    RolesUserRoles: TRolesUserRolesDB;
    PrivilegesRolePrivileges: TPrivilegesRolePrivilegesDB;
    constructor Create;
    destructor Destroy; override;
    function TryConnect: Boolean;
  published
  end;

implementation

uses
    Constants
  ;

{ TDatabase }

constructor TDatabase.Create;
begin
  DBConnection := TZConnection.Create(Nil);
  DBConnection.HostName := DEFAULT_HOSTNAME;
  DBConnection.Database := DEFAULT_DATABASE;
  DBConnection.Password := DEFAULT_CONNECTION_PASSWORD;
  DBConnection.Protocol := DEFAULT_CONNECTION_PROTOCOL;
  DBConnection.User := PROJECT_NAME;

  Users := TUsersDB.Create(DBConnection);
  Versions := TVersionsDB.Create(DBConnection);
  Parties := TPartiesDB.Create(DBConnection);
  Roles := TRolesDB.Create(DBConnection);
  UserRoles := TUserRolesDB.Create(DBConnection);
  Privileges := TPrivilegesDB.Create(DBConnection);
  RolePrivileges := TRolePrivilegesDB.Create(DBConnection);
  UsersUserRolesRolesRolePrivilegesPrivileges :=
    TUsersUserRolesRolesRolePrivilegesPrivilegesDB.Create(DBConnection);
  Persons := TPersonsDB.Create(DBConnection);
  RolesUserRoles := TRolesUserRolesDB.Create(DBConnection);
  PrivilegesRolePrivileges := TPrivilegesRolePrivilegesDB.Create(DBConnection);
end;

destructor TDatabase.Destroy;
begin
  if Assigned(PrivilegesRolePrivileges) then FreeAndNil(PrivilegesRolePrivileges);
  if Assigned(RolesUserRoles) then FreeAndNil(RolesUserRoles);
  if Assigned(Persons) then FreeAndNil(Persons);
  if Assigned(UsersUserRolesRolesRolePrivilegesPrivileges) then
    FreeAndNil(UsersUserRolesRolesRolePrivilegesPrivileges);
  if Assigned(RolePrivileges) then FreeAndNil(RolePrivileges);
  if Assigned(Privileges) then FreeAndNil(Privileges);
  if Assigned(UserRoles) then FreeAndNil(UserRoles);
  if Assigned(Roles) then FreeAndNil(Roles);
  if Assigned(Parties) then FreeAndNil(Parties);
  if Assigned(Versions) then FreeAndNil(Versions);
  if Assigned(Users) then FreeAndNil(Users);
  if Assigned(DBConnection) then FreeAndNil(DBConnection);
  inherited;
end;

function TDatabase.TryConnect: Boolean;
begin
  try
    DBConnection.Connected := False;
    DBConnection.Connect;
  except
  end;
  Result := DBConnection.Connected;
end;

end.
