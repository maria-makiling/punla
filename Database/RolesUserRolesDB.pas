unit RolesUserRolesDB; //roles table + user_roles table

interface

uses
    ZConnection
  , ZDataset
  , ProjectTypes
  ;

{$M+}

type
  TRolesUserRolesDB = class
  private
    FDBConnection: TZConnection;
  public
    constructor Create(const AConnection: TZConnection);
    function GetRolesUsers(const ARoleID: Integer): TZQuery;
    function GetUsersAvailableRoles(const AUserID: Integer): TZQuery;
    function GetUsersRoles(const AUserID: Integer): TZQuery;
    function SelectAllUsersAvailableRolesCreateArray(
      const AUserID: Integer): TRoles;
    function SelectAllUsersRolesCreateArray(const AUserID: Integer): TRoles;
  published
  end;

implementation

uses
    Constants
  , Utils
  ;

{ TRolesUserRolesDB }

constructor TRolesUserRolesDB.Create(const AConnection: TZConnection);
begin
  FDBConnection := AConnection;
end;

function TRolesUserRolesDB.GetRolesUsers(const ARoleID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
{
SELECT users.*
  FROM user_roles
  LEFT JOIN users ON user_roles.user_id=users.party_id
  WHERE role_id=7;
}
    SQL.SQL.Text :=
      'SELECT ' +
        USERS_DB_TABLENAME + '.* ' +
      'FROM ' +
        USERROLES_DB_TABLENAME + ' ' +
      'LEFT JOIN ' +
        USERS_DB_TABLENAME + ' ' +
          'ON ' +
            USERROLES_DB_TABLENAME + '.' + USERROLES_DB_USER_ID + '=' +
            USERS_DB_TABLENAME + '.' + USERS_DB_PARTY_ID + ' ' +
      'WHERE ' +
        USERROLES_DB_ROLE_ID + '=:' + USERROLES_DB_ROLE_ID
      ;
    SQL.ParamByName(USERROLES_DB_ROLE_ID).AsInteger := ARoleID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TRolesUserRolesDB.GetUsersAvailableRoles(const AUserID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
    {SELECT * FROM roles
    WHERE id NOT IN (
      SELECT role_id
      FROM user_roles
      WHERE user_id=1)
      AND is_active=1;}
    SQL.SQL.Text :=
      'SELECT * ' +
      'FROM ' +
        ROLES_DB_TABLENAME + ' ' +
      'WHERE ' +
        ROLES_DB_ID + ' NOT IN ' +
      '( ' +
        'SELECT ' +
          USERROLES_DB_ROLE_ID + ' ' +
        'FROM ' +
          USERROLES_DB_TABLENAME + ' ' +
        'WHERE ' +
          USERROLES_DB_USER_ID + '=:' + USERROLES_DB_USER_ID +
      ') AND ' +
        DB_IS_ACTIVE + '=1'
      ;
    SQL.ParamByName(USERROLES_DB_USER_ID).AsInteger := AUserID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TRolesUserRolesDB.GetUsersRoles(const AUserID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
    {SELECT roles.*
    FROM user_roles
    LEFT JOIN roles ON user_roles.role_id=roles.id
    WHERE user_id=:AUserID
    AND is_active=1;}
    SQL.SQL.Text :=
      'SELECT ' +
        ROLES_DB_TABLENAME + '.* ' +
      'FROM ' +
        USERROLES_DB_TABLENAME + ' ' +
      'LEFT JOIN ' +
        ROLES_DB_TABLENAME + ' ' +
          'ON ' +
            USERROLES_DB_TABLENAME + '.' + USERROLES_DB_ROLE_ID + '=' +
            ROLES_DB_TABLENAME + '.' + ROLES_DB_ID + ' ' +
      'WHERE ' +
        USERROLES_DB_USER_ID + '=:' + USERROLES_DB_USER_ID + ' ' +
      'AND ' +
        DB_IS_ACTIVE + '=1'
      ;
    SQL.ParamByName(USERROLES_DB_USER_ID).AsInteger := AUserID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TRolesUserRolesDB.SelectAllUsersAvailableRolesCreateArray(
  const AUserID: Integer): TRoles;
var
  SQL: TZQuery;
  Role: TRole;
  i: Integer;
begin
  Result := nil;

  SQL := GetUsersAvailableRoles(AUserID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Role := TRole.Create(
      SQL.FieldByName(ROLES_DB_ID).AsInteger,
      SQL.FieldByName(ROLES_DB_NAME).AsString,
      Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger)
    );
    Result[i] := Role;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TRolesUserRolesDB.SelectAllUsersRolesCreateArray(
  const AUserID: Integer): TRoles;
var
  SQL: TZQuery;
  Role: TRole;
  i: Integer;
begin
  Result := nil;
  SQL := GetUsersRoles(AUserID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Role := TRole.Create(
      SQL.FieldByName(ROLES_DB_ID).AsInteger,
      SQL.FieldByName(ROLES_DB_NAME).AsString,
      Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger)
    );
    Result[i] := Role;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

end.
