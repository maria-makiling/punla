unit PartiesDB;

interface

uses
    ZConnection
  , BaseDB
  ;

{$M+}

type
  TPartiesDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
  public
    function Edit(const AID: Integer; const ACreatedDate: TDateTime): Boolean;
    function Insert: Integer;
  published
  end;

implementation

uses
    Constants
  ;

{ TPartiesDB }

constructor TPartiesDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, PARTIES_DB_TABLENAME, PARTIES_DB_ID);
end;

function TPartiesDB.Insert: Integer;
begin
  Result := TBaseDB(Self).Insert;
end;

function TPartiesDB.Edit(const AID: Integer;
  const ACreatedDate: TDateTime): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [PARTIES_DB_CREATEDATE];
  Values := Values + [ACreatedDate];
  Result := TBaseDB(Self).Edit(Columns,Values,PARTIES_DB_ID,AID);
end;

end.
