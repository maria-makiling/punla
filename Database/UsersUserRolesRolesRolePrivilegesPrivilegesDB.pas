unit UsersUserRolesRolesRolePrivilegesPrivilegesDB; //users + user_roles + roles + role_privileges + privileges

interface

uses
    ZConnection
  , ZDataset
  ;

{$M+}

type
  TUsersUserRolesRolesRolePrivilegesPrivilegesDB = class
  private
    FDBConnection: TZConnection;
  public
    constructor Create(const AConnection: TZConnection);
    function GetUsersPrivileges(const AUserID: Integer): TZQuery;
  published
  end;

implementation

uses
    Constants
  ;

{ TUsersUserRolesRolesRolePrivilegesPrivilegesDB }

constructor TUsersUserRolesRolesRolePrivilegesPrivilegesDB.Create(const AConnection: TZConnection);
begin
  FDBConnection := AConnection;
end;

function TUsersUserRolesRolesRolePrivilegesPrivilegesDB.GetUsersPrivileges(
  const AUserID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
{
SELECT DISTINCT privileges.*
FROM users
LEFT JOIN user_roles ON user_roles.user_id=users.party_id
LEFT JOIN roles ON user_roles.role_id=roles.id
LEFT JOIN role_privileges ON role_privileges.role_id=roles.id
LEFT JOIN privileges ON role_privileges.privilege_id=privileges.id
WHERE party_id=1
ORDER BY privileges.id
}
    SQL.SQL.Text :=
      'SELECT DISTINCT ' +
        PRIVILEGES_DB_TABLENAME + '.* ' +
      'FROM ' +
        USERS_DB_TABLENAME + ' ' +
      'LEFT JOIN ' +
        USERROLES_DB_TABLENAME + ' ' +
          'ON ' +
            USERROLES_DB_TABLENAME + '.' + USERROLES_DB_USER_ID + '=' +
            USERS_DB_TABLENAME + '.' + USERS_DB_PARTY_ID + ' ' +
      'LEFT JOIN ' +
        ROLES_DB_TABLENAME + ' ' +
          'ON ' +
            USERROLES_DB_TABLENAME + '.' + USERROLES_DB_ROLE_ID + '=' +
            ROLES_DB_TABLENAME + '.' + ROLES_DB_ID + ' ' +
      'LEFT JOIN ' +
        ROLEPRIVILEGES_DB_TABLENAME + ' ' +
          'ON ' +
            ROLEPRIVILEGES_DB_TABLENAME + '.' + ROLEPRIVILEGES_DB_ROLE_ID + '=' +
            ROLES_DB_TABLENAME + '.' + ROLES_DB_ID + ' ' +
      'LEFT JOIN ' +
        PRIVILEGES_DB_TABLENAME + ' ' +
          'ON ' +
            ROLEPRIVILEGES_DB_TABLENAME + '.' + ROLEPRIVILEGES_DB_PRIVILEGE_ID + '=' +
            PRIVILEGES_DB_TABLENAME + '.' + PRIVILEGES_DB_ID + ' ' +
      'WHERE ' +
        USERS_DB_PARTY_ID + '=:' + USERS_DB_PARTY_ID + ' ' +
      'ORDER BY ' +
        PRIVILEGES_DB_TABLENAME + '.' + PRIVILEGES_DB_ID
      ;
    SQL.ParamByName(USERS_DB_PARTY_ID).AsInteger := AUserID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

end.
