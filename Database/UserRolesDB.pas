unit UserRolesDB;

interface

uses
    ZConnection
  , BaseDB
  ;

{$M+}

type
  TUserRolesDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
  public
    function Delete(const AUserID, ARoleID: Integer): Boolean;
    function Insert(const AUserID, ARoleID: Integer): Boolean;
  published
  end;

implementation

uses
    Constants
  ;

{ TUserRolesDB }

constructor TUserRolesDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, USERROLES_DB_TABLENAME,
    [USERROLES_DB_USER_ID] + [USERROLES_DB_ROLE_ID]);
end;

function TUserRolesDB.Delete(const AUserID, ARoleID: Integer): Boolean;
var
  IDs: Array of Variant;
begin
  IDs := nil;
  IDs := IDs + [AUserID];
  IDs := IDs + [ARoleID];
  Result := TBaseDB(Self).Delete(IDs);
end;

function TUserRolesDB.Insert(const AUserID, ARoleID: Integer): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [USERROLES_DB_USER_ID];
  Columns := Columns + [USERROLES_DB_ROLE_ID];
  Values := Values + [AUserID];
  Values := Values + [ARoleID];
  Result := TBaseDB(Self).Insert(Columns,Values) > 0;
end;

end.
