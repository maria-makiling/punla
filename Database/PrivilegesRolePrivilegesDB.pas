unit PrivilegesRolePrivilegesDB; //roles table + user_roles table

interface

uses
    ZConnection
  , ZDataset
  , ProjectTypes
  ;

{$M+}

type
  TPrivilegesRolePrivilegesDB = class
  private
    FDBConnection: TZConnection;
  public
    constructor Create(const AConnection: TZConnection);
    function GetRolesAvailablePrivileges(const ARoleID: Integer): TZQuery;
    function GetRolesPrivileges(const ARoleID: Integer): TZQuery;
    function SelectAllRolesAvailablePrivilegesCreateArray(
      const ARoleID: Integer): TPrivileges;
    function SelectAllRolesPrivilegesCreateArray(
      const ARoleID: Integer): TPrivileges;
  published
  end;

implementation

uses
    Constants
  ;

{ TPrivilegesRolePrivilegesDB }

constructor TPrivilegesRolePrivilegesDB.Create(const AConnection: TZConnection);
begin
  FDBConnection := AConnection;
end;

function TPrivilegesRolePrivilegesDB.GetRolesAvailablePrivileges(const ARoleID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
{
SELECT * FROM privileges
  WHERE id NOT IN (
	SELECT privilege_id
    FROM role_privileges
    WHERE role_id=2
  );
}
    SQL.SQL.Text :=
      'SELECT * ' +
      'FROM ' +
        PRIVILEGES_DB_TABLENAME + ' ' +
      'WHERE ' +
        PRIVILEGES_DB_ID + ' NOT IN ' +
      '( ' +
        'SELECT ' +
          ROLEPRIVILEGES_DB_PRIVILEGE_ID + ' ' +
        'FROM ' +
          ROLEPRIVILEGES_DB_TABLENAME + ' ' +
        'WHERE ' +
          ROLEPRIVILEGES_DB_ROLE_ID + '=:' + ROLEPRIVILEGES_DB_ROLE_ID +
      ')'
      ;
    SQL.ParamByName(ROLEPRIVILEGES_DB_ROLE_ID).AsInteger := ARoleID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TPrivilegesRolePrivilegesDB.GetRolesPrivileges(const ARoleID: Integer): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
{
SELECT privileges.*
FROM role_privileges
LEFT JOIN privileges ON role_privileges.privilege_id=privileges.id
WHERE role_id=2;
}
    SQL.SQL.Text :=
      'SELECT ' +
        PRIVILEGES_DB_TABLENAME + '.* ' +
      'FROM ' +
        ROLEPRIVILEGES_DB_TABLENAME + ' ' +
      'LEFT JOIN ' +
        PRIVILEGES_DB_TABLENAME + ' ' +
          'ON ' +
            ROLEPRIVILEGES_DB_TABLENAME + '.' + ROLEPRIVILEGES_DB_PRIVILEGE_ID + '=' +
            PRIVILEGES_DB_TABLENAME + '.' + PRIVILEGES_DB_ID + ' ' +
      'WHERE ' +
        ROLEPRIVILEGES_DB_ROLE_ID + '=:' + ROLEPRIVILEGES_DB_ROLE_ID
      ;
    SQL.ParamByName(ROLEPRIVILEGES_DB_ROLE_ID).AsInteger := ARoleID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TPrivilegesRolePrivilegesDB.SelectAllRolesAvailablePrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
var
  SQL: TZQuery;
  Privilege: TPrivilege;
  i: Integer;
begin
  Result := nil;

  SQL := GetRolesAvailablePrivileges(ARoleID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Privilege := TPrivilege.Create(
      SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger,
      SQL.FieldByName(PRIVILEGES_DB_NAME).AsString,
      SQL.FieldByName(PRIVILEGES_DB_DESCRIPTION).AsString
    );
    Result[i] := Privilege;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

function TPrivilegesRolePrivilegesDB.SelectAllRolesPrivilegesCreateArray(
  const ARoleID: Integer): TPrivileges;
var
  SQL: TZQuery;
  Privilege: TPrivilege;
  i: Integer;
begin
  Result := nil;
  SQL := GetRolesPrivileges(ARoleID);
  if (SQL = nil) or (SQL.RecordCount <= 0) then exit;

  SQL.Last;
  SetLength(Result, SQL.RecordCount);
  SQL.First;
  i := 0;
  while not SQL.Eof do
  begin
    Privilege := TPrivilege.Create(
      SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger,
      SQL.FieldByName(PRIVILEGES_DB_NAME).AsString,
      SQL.FieldByName(PRIVILEGES_DB_DESCRIPTION).AsString
    );
    Result[i] := Privilege;
    Inc(i);
    SQL.Next;
  end;

  SQL.Free;
end;

end.
