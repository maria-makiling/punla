unit VersionDB;

interface

uses
    ZConnection
  ;

{$M+}

type
  TVersionsDB = class
  private
    FDBMajorVersion: Integer;
    FDBMinorVersion: Integer;
    FAppMajorVersion: Integer;
    FAppMinorVersion: Integer;
    FAppBuildVersion: Integer;
    FAppRevisionVersion: Integer;
    FDBConnection: TZConnection;
  public
    constructor Create(const AConnection: TZConnection);
    function FetchVersion: Boolean;
  published
    property DBMajorVersion: Integer read FDBMajorVersion write FDBMajorVersion;
    property DBMinorVersion: Integer read FDBMinorVersion write FDBMinorVersion;
    property AppMajorVersion: Integer read FAppMajorVersion write FAppMajorVersion;
    property AppMinorVersion: Integer read FAppMinorVersion write FAppMinorVersion;
    property AppBuildVersion: Integer read FAppBuildVersion write FAppBuildVersion;
    property AppRevisionVersion: Integer read FAppRevisionVersion write FAppRevisionVersion;
  end;

implementation

uses
    ZDataset
  , Constants
  ;

{ TVersionsDB }

constructor TVersionsDB.Create(const AConnection: TZConnection);
begin
  FDBConnection := AConnection;
end;

function TVersionsDB.FetchVersion: Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := FDBConnection;
    SQL.SQL.Text :=
      'SELECT ' +
        VERSIONS_DB_DATABASE_MAJOR + ', ' +
        VERSIONS_DB_DATABASE_MINOR + ', ' +
        VERSIONS_DB_APPLICATION_MAJOR + ', ' +
        VERSIONS_DB_APPLICATION_MINOR + ', ' +
        VERSIONS_DB_APPLICATION_BUILD + ', ' +
        VERSIONS_DB_APPLICATION_REVISION + ', ' +
        DB_CREATED_DATE + ' ' +
      'FROM ' +
        VERSIONS_DB_TABLENAME + ' ' +
      'ORDER BY ' +
        VERSIONS_DB_ID + ' ' +
      'DESC LIMIT 1'
      ;
    SQL.Open;
    while not SQL.Eof do begin
      FDBMajorVersion := SQL.FieldByName(VERSIONS_DB_DATABASE_MAJOR).AsInteger;
      FDBMinorVersion := SQL.FieldByName(VERSIONS_DB_DATABASE_MINOR).AsInteger;
      FAppMajorVersion := SQL.FieldByName(VERSIONS_DB_APPLICATION_MAJOR).AsInteger;
      FAppMinorVersion := SQL.FieldByName(VERSIONS_DB_APPLICATION_MINOR).AsInteger;
      FAppBuildVersion := SQL.FieldByName(VERSIONS_DB_APPLICATION_BUILD).AsInteger;
      FAppRevisionVersion := SQL.FieldByName(VERSIONS_DB_APPLICATION_REVISION).AsInteger;
      Result := True;
      Break;
    end;
  finally
    SQL.Free;
  end;
end;

end.
