unit PrivilegesDB;

interface

uses
    ZConnection
  , ZDataset
  , BaseDB
  ;

{$M+}

type
  TPrivilegesDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
    function GetID(const AName: String): Integer;
  published
  end;

implementation

uses
    Constants
  ;

{ TPrivilegesDB }

constructor TPrivilegesDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, PRIVILEGES_DB_TABLENAME, PRIVILEGES_DB_ID);
end;

function TPrivilegesDB.GetID(const AName: String): Integer;
var
  Columns: Array of String;
  Values: Array of Variant;
  SQL: TZQuery;
begin
  Result := -1;
  Columns := [PRIVILEGES_DB_NAME];
  Values := [AName];
  SQL := SelectWhereCreateQry(Columns,Values,'');
  if SQL = nil then
    Exit;

  if (SQL.RecordCount > 0) and
    (SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger > 0) then
  begin
    LastID := SQL.FieldByName(PRIVILEGES_DB_ID).AsInteger;
    Result := LastID;
  end;
  SQL.Free;
end;

end.

