unit RolesDB;

interface

uses
    ZConnection
  , BaseDB
  ;

{$M+}

type
  TRolesDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
  public
    function Edit(const AID: Integer; const AName: String): Boolean;
    function GetName(const AID: Integer): String;
    function Insert(const AName: String): Integer;
    function IsRoleNameUnique(const ARoleName: String): Boolean;
    function SetName(const AID: Integer; const AName: String): Boolean;
  published
  end;

implementation

uses
    ZDataset
  , Constants
  ;


{ TRolesDB }

constructor TRolesDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, ROLES_DB_TABLENAME, ROLES_DB_ID);
end;

function TRolesDB.Edit(const AID: Integer; const AName: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [ROLES_DB_NAME];
  Values := Values + [AName];
  Result := TBaseDB(Self).Edit(Columns,Values,ROLES_DB_ID,AID);
end;

function TRolesDB.GetName(const AID: Integer): String;
var
  SQL: TZQuery;
begin
  Result := '';
  SQL := TBaseDB(Self).Select(AID);
  if SQL = nil then
    Exit;

  try
    Result := SQL.FieldByName(ROLES_DB_NAME).AsString;
  finally
    SQL.Free;
  end;
end;

function TRolesDB.Insert(const AName: String): Integer;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [ROLES_DB_NAME];
  Values := Values + [AName];
  Result := TBaseDB(Self).Insert(Columns,Values);
end;

function TRolesDB.IsRoleNameUnique(const ARoleName: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
  SQL: TZQuery;
begin
  Result := True;
  Columns := [ROLES_DB_NAME];
  Values := [ARoleName];
  SQL := SelectWhereCreateQry(Columns,Values,'');
  if SQL = nil then
    Exit;

  if (SQL.RecordCount > 0)  then
    Result := False;

  SQL.Free;
end;

function TRolesDB.SetName(const AID: Integer; const AName: String): Boolean;
begin
  Result := Edit(AID, AName);
end;

end.

