unit PersonsDB;

interface

uses
    ZConnection
  , BaseDB
  , ProjectTypes
  ;

{$M+}

type
  TPersonsDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
  public
    function Edit(const APartyID: Integer; const AGivenName,
      ASurname: String): Boolean;
    function GetName(const APartyID: Integer): String;
    function GetPerson(const APartyID: Integer): TPerson;
    function Insert(const APartyID: Integer; const AGivenName: String;
      const ASurname: String): Integer;
  published
  end;

implementation

uses
    Constants
  , ZDataset
  ;

{ TPersonsDB }

constructor TPersonsDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, PERSONS_DB_TABLENAME, PERSONS_DB_PARTY_ID);
end;

function TPersonsDB.Edit(const APartyID: Integer; const AGivenName,
  ASurname: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [PERSONS_DB_GIVEN_NAME];
  Columns := Columns + [PERSONS_DB_SURNAME];
  Values := Values + [AGivenName];
  Values := Values + [ASurname];
  Result := TBaseDB(Self).Edit(Columns,Values,PERSONS_DB_PARTY_ID,APartyID);
end;

function TPersonsDB.GetName(const APartyID: Integer): String;
var
  SQL: TZQuery;
begin
  Result := '';
  SQL := TBaseDB(Self).Select(APartyID);
  if SQL = nil then
    Exit;

  Result :=
    SQL.FieldByName(PERSONS_DB_GIVEN_NAME).AsString + ' ' +
    SQL.FieldByName(PERSONS_DB_SURNAME).AsString;
  SQL.Free;
end;

function TPersonsDB.GetPerson(const APartyID: Integer): TPerson;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TBaseDB(Self).Select(APartyID);
  if SQL = nil then
    Exit;

  Result := TPerson.Create(
    APartyID,
    SQL.FieldByName(PERSONS_DB_GIVEN_NAME).AsString,
    SQL.FieldByName(PERSONS_DB_SURNAME).AsString
  );
  SQL.Free;
end;

function TPersonsDB.Insert(const APartyID: Integer; const AGivenName,
  ASurname: String): Integer;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [PERSONS_DB_PARTY_ID];
  Columns := Columns + [PERSONS_DB_GIVEN_NAME];
  Columns := Columns + [PERSONS_DB_SURNAME];
  Values := Values + [APartyID];
  Values := Values + [AGivenName];
  Values := Values + [ASurname];
  Result := TBaseDB(Self).Insert(Columns,Values,APartyID);
end;

end.
