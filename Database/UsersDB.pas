unit UsersDB;

interface

uses
    BaseDB
  , ZConnection
  ;

{$M+}

type
  TUsersDB = class(TBaseDB)
  private
    FUsername: String;
  public
    constructor Create(const AConnection: TZConnection);
  public
    function ChangePartyID(const AOldPartyID, ANewPartyID: Integer): Boolean;
    function GetPartyID(const AUsername: String; const APassword: String): Integer;
    function IsUsernameUnique(const ASearch: String): Boolean;
    function Insert(const APartyID: Integer; const AUsername: String; const APassword: String): Integer;
    function Edit(const APartyID: Integer; const AUsername, APassword: String): Boolean;
    function EditPassword(const APartyID: Integer; const APassword: String): Boolean;
    function EditUsername(const APartyID: Integer; const AUsername: String): Boolean;
  published
    property Username: String read FUsername;
  end;

implementation

uses
    ZDataset
  , Constants
  , Bcrypt
  ;

function TUsersDB.ChangePartyID(const AOldPartyID,
  ANewPartyID: Integer): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  if not IsExist(AOldPartyID) then
    Exit;

  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'UPDATE ' +
        USERS_DB_TABLENAME + ' ' +
      'SET ' +
        USERS_DB_PARTY_ID + '=:New' + USERS_DB_PARTY_ID + ' ' +
      'WHERE ' +
        USERS_DB_PARTY_ID + '=:Old' + USERS_DB_PARTY_ID
      ;
    SQL.ParamByName('New' + USERS_DB_PARTY_ID).AsInteger := ANewPartyID;
    SQL.ParamByName('Old' + USERS_DB_PARTY_ID).AsInteger := AOldPartyID;
    SQL.ExecSQL;
    if IsExist(AOldPartyID) then
      Exit;
    if IsExist(ANewPartyID) then
    begin
      LastID := ANewPartyID;
      Result := True;
    end;
  finally
    SQL.Free;
  end;
end;

constructor TUsersDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, USERS_DB_TABLENAME, USERS_DB_PARTY_ID);
end;

function TUsersDB.Edit(const APartyID: Integer; const AUsername,
  APassword: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [USERS_DB_USERNAME];
  Columns := Columns + [USERS_DB_PASSWORD];
  Values := Values + [AUsername];
  Values := Values + [TBCrypt.HashPassword(APassword)];
  Result := TBaseDB(Self).Edit(Columns,Values,USERS_DB_PARTY_ID,APartyID);
end;

function TUsersDB.EditPassword(const APartyID: Integer;
  const APassword: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [USERS_DB_PASSWORD];
  Values := Values + [TBCrypt.HashPassword(APassword)];
  Result := TBaseDB(Self).Edit(Columns,Values,USERS_DB_PARTY_ID,APartyID);
end;

function TUsersDB.EditUsername(const APartyID: Integer;
  const AUsername: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [USERS_DB_USERNAME];
  Values := Values + [AUsername];
  Result := TBaseDB(Self).Edit(Columns,Values,USERS_DB_PARTY_ID,APartyID);
end;

function TUsersDB.GetPartyID(const AUsername, APassword: String): Integer;
var
  SQL: TZQuery;
  bRehash: boolean;
begin
  Result := -1;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT ' +
        USERS_DB_PARTY_ID + ', ' +
        USERS_DB_PASSWORD + ' ' +
      'FROM ' +
        USERS_DB_TABLENAME + ' ' +
      'WHERE ' +
        USERS_DB_USERNAME + '=:' + USERS_DB_USERNAME + ' ' +
      'AND ' +
        DB_IS_ACTIVE + '=1'
      ;
    SQL.ParamByName(USERS_DB_USERNAME).AsString := AUsername;
    SQL.Open;
    SQL.First;
    while not SQL.Eof do begin
      if (TBCrypt.CheckPassword
          (APassword,
          SQL.FieldByName(USERS_DB_PASSWORD).AsString,
          bRehash
        )) then begin
        LastID := SQL.FieldByName(USERS_DB_PARTY_ID).AsInteger;
        Result := LastID;
        FUsername := AUsername;
        Break;
      end;
      SQL.Next;
    end;
  finally
    SQL.Free;
  end;
end;

function TUsersDB.Insert(const APartyID: Integer; const AUsername: String; const APassword: String): Integer;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [USERS_DB_PARTY_ID];
  Columns := Columns + [USERS_DB_USERNAME];
  Columns := Columns + [USERS_DB_PASSWORD];
  Values := Values + [APartyID];
  Values := Values + [AUsername];
  Values := Values + [TBCrypt.HashPassword(APassword)];
  Result := TBaseDB(Self).Insert(Columns,Values);
  if Result > 0 then
    FUsername := AUsername;
end;

function TUsersDB.IsUsernameUnique(const ASearch: String): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
  SQL: TZQuery;
begin
  Result := True;
  Columns := [USERS_DB_USERNAME];
  Values := [ASearch];
  SQL := SelectWhereCreateQry(Columns,Values,'');
  if SQL = nil then
    Exit;

  if (SQL.RecordCount > 0)  then
    Result := False;

  SQL.Free;
end;

end.
