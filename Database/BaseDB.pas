unit BaseDB;

interface

uses
    Global
  , ProjectTypes
  , ZConnection
  , ZDataset
  ;

type
  TBaseDB = class
  private
    FTableName: String;
    FIDColumn: String;
    FIDColumns: TStringArray;
    FIsSingleColumnID: Boolean;
  public
    DBConnection: TZConnection;
    LastID: Variant;
    LastIDs: Array of Variant;
  private
    function SetLastIDs(const IDs: Array of Variant): Boolean;
    function SQLStringBuilder(const ArrStr: Array of String; const Separator: String): String; Overload;
    function SQLStringBuilder(const ArrStr: Array of String): String; Overload;
    function SQLStringBuilderSet(const ArrStr: Array of String): String;
    function SQLStringBuilderWhere(const ArrStr: array of String; const Logic: String = 'And'): String;
    function SQLParamBuilder(Qry: TZQuery; const ArrStr: Array of String; const ArrVar: Array of Variant): String; Overload;
  public
    constructor Create(const AConnection: TZConnection;
      const ATableName: String; const AIDColumn: String); overload;
    constructor Create(const AConnection: TZConnection;
      const ATableName: String; const AIDColumns: TStringArray); overload;
    function Delete(const AID: Variant): Boolean; overload;
    function Delete(const AIDs: Array of Variant): Boolean; overload;
    function Edit(const AColumns: Array of String;
      const AValues: Array of Variant; const AIDColumn: String;
      const AID: Variant): Boolean;
    function GetCount: Integer;
    function GetIsActive(const AID: Variant): Boolean;
    function GetLastInsertID: Integer;
    function Insert: Integer; overload;
    function Insert(const AColumns: Array of String;
      const AValues: Array of Variant): Integer; overload;
    function Insert(const AColumns: Array of String;
      const AValues: Array of Variant; const AID: Variant): Integer; overload;
    function IsExist(const AID: Variant): Boolean; overload;
    function IsExist(const AIDs: Array of Variant): Boolean; overload;
    function SelectAllCreateQry: TZQuery;
    function SelectAllActiveCreateQry: TZQuery;
    function SelectWhereCreateQry(const AColumns: Array of String;
      const AValues: Array of Variant; const ALogic: String): TZQuery;
    function Select(const AID: Variant): TZQuery;
    function SetIsActive(const AID: Variant; const AValue: Boolean): Boolean;
  end;

implementation

uses
    Utils
  , Constants
  ;

constructor TBaseDB.Create(const AConnection: TZConnection;
      const ATableName: String; const AIDColumn: String);
begin
  DBConnection := AConnection;
  FTableName := ATableName;
  FIDColumn := AIDColumn;
  FIsSingleColumnID := True;
end;

constructor TBaseDB.Create(const AConnection: TZConnection;
  const ATableName: String; const AIDColumns: TStringArray);
begin
  DBConnection := AConnection;
  FTableName := ATableName;
  FIDColumns := AIDColumns;
  FIsSingleColumnID := False;
end;

function TBaseDB.Edit(const AColumns: array of String;
  const AValues: array of Variant; const AIDColumn: String;
  const AID: Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  if not IsExist(AID) then
    Exit;

  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'UPDATE ' +
        FTableName + ' ' +
      'SET ' +
        SQLStringBuilderSet(AColumns) +
      'WHERE ' +
        AIDColumn + '=:' + AIDColumn
      ;
    SQL.ParamByName(AIDColumn).AsInteger := AID;
    SQLParamBuilder(SQL, AColumns, AValues);
    SQL.ExecSQL;
    Result := True;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.GetCount: Integer;
var
  SQL: TZQuery;
begin
  Result := -1;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text := 'SELECT COUNT(*) as count FROM ' + FTableName;
    SQL.Open;
    Result := SQL.FieldByName('count').AsInteger;
    SQL.Close;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.GetIsActive(const AID: Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  SQL := Select(AID);
  if SQL = nil then
    Exit;

  try
    Result := Utils.IntToBool(SQL.FieldByName(DB_IS_ACTIVE).AsInteger);
  finally
    SQL.Free;
  end;
end;

function TBaseDB.GetLastInsertID: Integer;
var
  SQL: TZQuery;
begin
  Result := -1;
  if FIsSingleColumnID then
  begin
    SQL := TZQuery.Create(nil);
    try
      SQL.Connection := DBConnection;
      SQL.SQL.Text := 'SELECT LAST_INSERT_ID()';
      SQL.Open;
      Result := SQL.Fields[0].AsInteger;
      SQL.Close;
    finally
      SQL.Free;
    end;
  end
  else
    Result := 1; {Assign 1 because for tables with multi column IDs, this
      method is useless and just represents a successful operation}
end;

function TBaseDB.Insert: Integer;
var
  SQL: TZQuery;
begin
  Result := -1;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'INSERT INTO ' +
        FTableName + '() VALUES ()';
    SQL.ExecSQL;
    LastID := GetLastInsertID;
    Result := LastID;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.Insert(const AColumns: array of String;
  const AValues: array of Variant; const AID: Variant): Integer;
var
  SQL: TZQuery;
begin
  Result := -1;
  if IsExist(AID) then
    Exit;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'INSERT INTO ' +
        FTableName + ' ' +
      '(' +
        SQLStringBuilder(AColumns) +
      ') ' +
      'VALUES ' +
      '(' +
        SQLStringBuilder(AColumns, ':') +
      ')';
    SQLParamBuilder(SQL,AColumns,AValues);
    SQL.ExecSQL;
    LastID := GetLastInsertID;
    Result := LastID;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.Insert(const AColumns: array of String;
  const AValues: array of Variant): Integer;
var
  SQL: TZQuery;
begin
  Result := -1;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'INSERT INTO ' +
        FTableName + ' ' +
      '(' +
        SQLStringBuilder(AColumns) +
      ') ' +
      'VALUES ' +
      '(' +
        SQLStringBuilder(AColumns, ':') +
      ')';
    SQLParamBuilder(SQL,AColumns,AValues);
    SQL.ExecSQL;
    LastID := GetLastInsertID;
    Result := LastID;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.IsExist(const AID: Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT * FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        FIDColumn + '=:' + FIDColumn
      ;
    SQL.ParamByName(FIDColumn).Value := AID;
    SQL.Open;
    if SQL.RecordCount > 0 then
    begin
      Result := True;
      LastID := AID;
    end;
  finally
    SQL.Free;
  end;
end;

//AIDs order is dependent on the order used during Create
function TBaseDB.IsExist(const AIDs: array of Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT * FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        SQLStringBuilderWhere(FIDColumns)
      ;
    SQLParamBuilder(SQL, FIDColumns, AIDs);
    SQL.Open;
    if SQL.RecordCount > 0 then
    begin
      Result := True;
      SetLastIDs(AIDs);
    end;
  finally
    SQL.Free;
  end;
end;

function TBaseDB.Select(const AID: Variant): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  if not IsExist(AID) then
    Exit;

  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT * FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        FIDColumn + '=:' + FIDColumn
      ;
    SQL.ParamByName(FIDColumn).AsInteger := AID;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TBaseDB.SelectAllActiveCreateQry: TZQuery;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := [DB_IS_ACTIVE];
  Values := [1];
  Result := SelectWhereCreateQry(Columns,Values,'');
end;

function TBaseDB.SelectAllCreateQry: TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT * FROM ' +
        FTableName
      ;
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TBaseDB.SelectWhereCreateQry(const AColumns: array of String;
  const AValues: array of Variant; const ALogic: String): TZQuery;
var
  SQL: TZQuery;
begin
  Result := nil;
  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'SELECT * FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        SQLStringBuilderWhere(AColumns, ALogic)
      ;
    SQLParamBuilder(SQL, AColumns, AValues);
    SQL.Open;
    if SQL.RecordCount > 0 then
      Result := SQL;
  except
    SQL.Free;
  end;
end;

function TBaseDB.SetIsActive(const AID: Variant;
  const AValue: Boolean): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [DB_IS_ACTIVE];
  Values := Values + [Utils.BoolToInt(AValue)];
  Result := Edit(Columns,Values,FIDColumn,AID);
end;

function TBaseDB.SetLastIDs(const IDs: array of Variant): Boolean;
begin
  Result := False;
end;

function TBaseDB.SQLParamBuilder(Qry: TZQuery; const ArrStr: array of String;
  const ArrVar: array of Variant): String;
var
  i: Integer;
begin
  for i := 0 to Length(ArrStr) - 1 do
  begin
    Qry.ParamByName(ArrStr[i]).Value := ArrVar[i];
  end;
end;

function TBaseDB.SQLStringBuilder(
  const ArrStr: array of String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(ArrStr) - 1 do
  begin
    if i < Length(ArrStr) - 1 then
      Result := Result + ArrStr[i] + ', '
    else
      Result := Result + ArrStr[i] + ' ';
  end;
end;

function TBaseDB.SQLStringBuilderSet(const ArrStr: array of String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(ArrStr) - 1 do
  begin
    if i < Length(ArrStr) - 1 then
      Result := Result + ArrStr[i] + '=:' + ArrStr[i] + ', '
    else
      Result := Result + ArrStr[i] + '=:' + ArrStr[i] + ' ';
  end;
end;

function TBaseDB.SQLStringBuilderWhere(const ArrStr: array of String;
  const Logic: String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(ArrStr) - 1 do
  begin
    if i < Length(ArrStr) - 1 then
      Result := Result + ArrStr[i] + '=:' + ArrStr[i] + ' ' + Logic + ' '
    else
      Result := Result + ArrStr[i] + '=:' + ArrStr[i] + ' ';
  end;
end;

function TBaseDB.SQLStringBuilder(const ArrStr: array of String;
  const Separator: String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(ArrStr) - 1 do
  begin
    if i < Length(ArrStr) - 1 then
      Result := Result + Separator + ArrStr[i] + ', '
    else
      Result := Result + Separator + ArrStr[i] + ' ';
  end;
end;

function TBaseDB.Delete(const AID: Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  if not IsExist(AID) then
    Exit;

  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'DELETE FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        FIDColumn + '=:' + FIDColumn
      ;
    SQL.ParamByName(FIDColumn).AsInteger := AID;
    SQL.ExecSQL;
    if IsExist(AID) then
      Exit;
    Result := True;
  finally
    SQL.Free;
  end;
end;

//AIDs order is dependent on the order used during Create
function TBaseDB.Delete(const AIDs: array of Variant): Boolean;
var
  SQL: TZQuery;
begin
  Result := False;
  if not IsExist(AIDs) then
    Exit;

  SQL := TZQuery.Create(nil);
  try
    SQL.Connection := DBConnection;
    SQL.SQL.Text :=
      'DELETE FROM ' +
        FTableName + ' ' +
      'WHERE ' +
        SQLStringBuilderWhere(FIDColumns)
      ;
    SQLParamBuilder(SQL, FIDColumns, AIDs);
    SQL.ExecSQL;
    if IsExist(AIDs) then
      Exit;
    Result := True;
  finally
    SQL.Free;
  end;
end;

end.
