unit RolePrivilegesDB;

interface

uses
    ZConnection
  , BaseDB
  ;

{$M+}

type
  TRolePrivilegesDB = class(TBaseDB)
  private
  public
    constructor Create(const AConnection: TZConnection);
  public
    function Delete(const ARoleID, APrivilegeID: Integer): Boolean;
    function Insert(const ARoleID, APrivilegeID: Integer): Boolean;
  published
  end;

implementation

uses
    Constants
  ;

{ TRolePrivilegesDB }

constructor TRolePrivilegesDB.Create(const AConnection: TZConnection);
begin
  inherited Create(AConnection, ROLEPRIVILEGES_DB_TABLENAME,
    [ROLEPRIVILEGES_DB_ROLE_ID] + [ROLEPRIVILEGES_DB_PRIVILEGE_ID]);
end;

function TRolePrivilegesDB.Delete(const ARoleID, APrivilegeID: Integer): Boolean;
var
  IDs: Array of Variant;
begin
  IDs := nil;
  IDs := IDs + [ARoleID];
  IDs := IDs + [APrivilegeID];
  Result := TBaseDB(Self).Delete(IDs);
end;

function TRolePrivilegesDB.Insert(const ARoleID, APrivilegeID: Integer): Boolean;
var
  Columns: Array of String;
  Values: Array of Variant;
begin
  Columns := nil;
  Values := nil;
  Columns := Columns + [ROLEPRIVILEGES_DB_ROLE_ID];
  Columns := Columns + [ROLEPRIVILEGES_DB_PRIVILEGE_ID];
  Values := Values + [ARoleID];
  Values := Values + [APrivilegeID];
  Result := TBaseDB(Self).Insert(Columns,Values) > 0;
end;

end.

