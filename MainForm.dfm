object Main: TMain
  Left = 0
  Top = 0
  ClientHeight = 261
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mmMain
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sbStatus: TStatusBar
    Left = 0
    Top = 242
    Width = 505
    Height = 19
    Panels = <
      item
        Text = 'Username'
        Width = 150
      end
      item
        Text = 'ApplicationVersion'
        Width = 150
      end
      item
        Text = 'DatabaseVersion'
        Width = 150
      end>
  end
  object mmMain: TMainMenu
    Left = 32
    Top = 24
    object miUser: TMenuItem
      Caption = '&User'
      object miUserLogin: TMenuItem
        Caption = 'Login &New User'
        OnClick = miUserLoginClick
      end
      object miUserClose: TMenuItem
        Caption = '&Close'
      end
    end
    object miMaintenance: TMenuItem
      Caption = '&Maintenance'
      object miMaintenanceUser: TMenuItem
        Caption = '&Users'
        OnClick = miMaintenanceUserClick
      end
      object miMaintenanceRoles: TMenuItem
        Caption = '&Roles'
        OnClick = miMaintenanceRolesClick
      end
    end
  end
end
